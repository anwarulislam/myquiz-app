import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:quiz/helper/functions.dart';
import 'package:quiz/helper/instance_bindings.dart';
import 'package:quiz/sizeconfig.dart';
import 'package:quiz/views/home_page.dart';
import 'package:quiz/views/splash_screen.dart';
import 'package:quiz/views/welcome_page.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            SystemChrome.setPreferredOrientations([
              DeviceOrientation.portraitUp,
            ]);
            return GetMaterialApp(
              debugShowCheckedModeBanner: false,
              initialBinding: InstanceBinding(),
              home: SplashScreen(),
            );
          }

      );
    });

  }
}

// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   bool _isLoggedIn = false;
//   @override
//   void initState() {
//     checkUserLoggedInStatus();
//     super.initState();
//   }

//   checkUserLoggedInStatus() async {
//     HelperFunctions.getUserLoggedInDetails().then((value) {
//       setState(() {
//         _isLoggedIn = value;
//       });
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Quiz App',
//       debugShowCheckedModeBanner: false,
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         visualDensity: VisualDensity.adaptivePlatformDensity,
//       ),
//       home: (_isLoggedIn ?? false) ? HomePage() : WelcomePage(),
//     );
//   }
// }
