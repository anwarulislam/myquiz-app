import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/user_controller.dart';
import 'package:quiz/models/quiz_model.dart';
import 'package:quiz/models/solved_users_model.dart';
import 'package:quiz/models/user.dart';
import 'package:quiz/views/addquestion.dart';
import 'package:quiz/views/home_page.dart';

class SolvedUserServices{
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  Stream<List<SolvedUsersModel>> solverSteam(String id) {
    return _firestore
        .collection(QUIZ_COLLECTION).doc(id)
        .collection(SOLVED_USERS)
        .orderBy(DATE_CREATED, descending: true)
        .snapshots()
        .map((QuerySnapshot query) {
      List<SolvedUsersModel> retVal = List();
      query.docs.forEach((element) {
        retVal.add(SolvedUsersModel.fromDocumentSnapshot(doc:element));
      });
      return retVal;
    });
  }
  addSolver(String id,int score){
    DocumentReference _ref=_firestore.collection(USERS_COLLECTION)
        .doc(Get.find<UserController>().user.userId);
    _firestore.collection(QUIZ_COLLECTION).doc(id)
        .collection(SOLVED_USERS).doc().set({
      SOLVER_REFERENCE:_ref,
      SCORE:score,
      DATE_CREATED:DateTime.now().toString(),
    });
  }
  updateQuestions(Map map,String id)async{
    await  _firestore.collection(QUIZ_COLLECTION)
        .doc(id)
        .update({
      QUESTION_LIST:map[QUESTION_LIST]

    }).then((value)async{
      Get.snackbar("Success", "Quiz Updated");
      Get.off(HomePage());

    }).catchError((onError)=>
        Get.snackbar("Error While Update Info ", onError.message));
  }

  getSolvers(String qid){
    _firestore.collection(QUIZ_COLLECTION).doc(qid)
        .collection(SOLVED_USERS).get();
  }
}