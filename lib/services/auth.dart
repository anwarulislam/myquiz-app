// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:quiz/models/user.dart';
// import 'package:quiz/services/database.dart';
//
// class AuthService {
//   FirebaseAuth _auth = FirebaseAuth.instance;
//   FirebaseUser loggedInUser;
//   String userUid;
//
//   void getCurrentUser() async {
//     try {
//       final currentLoggedUser = await _auth.currentUser();
//       if (currentLoggedUser != null) {
//         loggedInUser = currentLoggedUser;
//
//         userUid = loggedInUser.uid;
//         // print(userUid);
//         // final userUUID = loggedInUser.uid;
//
//       }
//     } catch (e) {
//       print(e);
//     }
//   }
//
//   User _userFromFirebaseUser(FirebaseUser user) {
//     return user != null ? User(uid: user.uid) : null;
//   }
//
//   Future signInEmailAndPass(String email, String password) async {
//     try {
//       AuthResult authResult = await _auth.signInWithEmailAndPassword(
//           email: email, password: password);
//       FirebaseUser firebaseUser = authResult.user;
//       return _userFromFirebaseUser(firebaseUser);
//     } catch (e) {
//       print(e.toString());
//     }
//   }
//
//   Future signUpWithEmailAndPassword(
//       String name, String email, String password) async {
//     try {
//       AuthResult authResult = await _auth.createUserWithEmailAndPassword(
//           email: email, password: password);
//       FirebaseUser firebaseUser = authResult.user;
//
//       await DatabaseService(uid: firebaseUser.uid).updateUserData(name);
//       return _userFromFirebaseUser(firebaseUser);
//     } catch (e) {
//       print(e.toString());
//     }
//   }
//
//   Future signOut() async {
//     try {
//       return await _auth.signOut();
//     } catch (e) {
//       print(e.toString());
//       return null;
//     }
//   }
// }
