import 'dart:io';

class AdMobService {

  String getAdMobAppId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-7607609998503764~9198205736';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-7607609998503764~7792790574';
    }
    return null;
  }

  String getBannerAdId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-7607609998503764/8722728869';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-7607609998503764/7230517944';
    }
    return null;
  }
}