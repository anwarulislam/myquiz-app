import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/controllers/user_controller.dart';
import 'package:quiz/models/user.dart';
import 'package:quiz/services/auth.dart';

class DatabaseService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final String uid;
  DatabaseService({ this.uid });
  final CollectionReference userCollection = Firestore.instance.collection('users');

  // AuthService authservice = new AuthService();


  Future updateUserData(String name) async{
    return await userCollection.document(uid).setData({
      'name': name
    });
  }


  Future<UserModel> getUser(String uid) async {
    try {
      DocumentSnapshot _doc =
      await _firestore.collection(USERS_COLLECTION).doc(uid).get();

      return UserModel.fromDocumentSnapshot(doc: _doc);
    } catch (e) {
      print(e);
      rethrow;
    }
  }
  Stream<List<UserModel>> usersStream(String uid) {
    return _firestore
        .collection(USERS_COLLECTION)
    // .where(FOLLOWING_USER_ID,isEqualTo:uid)
        .orderBy(DATE_CREATED, descending: true)
        .snapshots()
        .map((QuerySnapshot query) {
      List<UserModel> retVal = List();
      query.docs.forEach((element) {


        if(element.id!=Get.find<FirebaseController>().auth.currentUser.uid){
          retVal.add(UserModel.fromDocumentSnapshot(doc:element));
        }

      });
      return retVal;
    });
  }
  updateUserImage( image)async{
    var snapshot  =await FirebaseStorage.instance
        .ref()
        .child('profile_images/${DateTime.now().toString() }myimage.jpg')
        .putFile(image);

    await snapshot.ref.getDownloadURL().then((value) {
      updateSingleField(fieldName: PROFILE_IMAGE,value: value);

    });
  }
  updateSingleField({String fieldName,String value })async{
    await  _firestore.collection(USERS_COLLECTION)
        .doc(Get.find<FirebaseController>().auth.currentUser.uid)
        .update({
      fieldName:value
    }).then((value)async{
      Get.snackbar("Success", "User Updated");
      Get.find<UserController>().user =
      await getUser(Get.find<FirebaseController>().auth.currentUser.uid);
    }).catchError((onError)=>
        Get.snackbar("Error While Update Info ", onError.message));
  }
  updateUser({Map map })async{
    await  _firestore.collection(USERS_COLLECTION)
        .doc(Get.find<FirebaseController>().auth.currentUser.uid)
        .update({
      NAME:map[NAME],
      LOCATION:map[LOCATION],
      USER_NAME:map[USER_NAME]
    }).then((value)async{
      Get.snackbar("Success", "User Updated");
      Get.find<UserController>().user =
      await getUser(Get.find<FirebaseController>().auth.currentUser.uid);
      Get.back();
      Get.back();
    });
  }

  // Future<void> addQuizData(String quizId, String quizImageUrl, String quizTitle, String quizDescription, String userUid ) async {
  //   await Firestore.instance
  //       .collection("Quiz")
  //       .document(quizId)
  //       .setData({"quizId": quizId,
  //       "quizImgUrl": quizImageUrl,
  //       "quizTitle": quizTitle,
  //       "quizDesc": quizDescription,
  //       "userInformation": Firestore.instance.collection("users").document(userUid)})
  //       .catchError((e) {
  //     print(e.toString());
  //   });
  // }
  //
  // Future<void> updateUserQuizData(String quizId, String quizTitle, String userUid) async {
  //   await Firestore.instance
  //       .collection("users")
  //       .document(userUid)
  //       .collection("quizId")
  //       .document(quizId).setData({
  //     "quizCreator": Firestore.instance.collection("users").document(userUid),
  //     "quizTitle": quizTitle,
  //     "score": "0",
  //     "rank": "1st"
  //   }).catchError((e){
  //     print(e.toString());
  //   });
  // }

  // add result to database
  // Future<void> addUserResult(Map questionResult, String quizId) async {
  //   await Firestore.instance.collection("Quiz").document(quizId).collection("Result")
  //       .add(questionResult).catchError((e){
  //     print("database user Result");
  //     print(e);
  //   });
  // }

  // Future<void> addQuestionData(Map questionData, String quizId) async {
  //   await Firestore.instance.collection("Quiz").document(quizId).collection("QNA")
  //   .add(questionData).catchError((e){
  //     print(e);
  //   });
  // }

  getQuizData() async{
    return await Firestore.instance.collection("Quiz").snapshots();
  }

  // getUserResultData() async {
  //
  //   authservice.getCurrentUser();
  //   final currentUid = authservice.loggedInUser.uid;
  //   final quizes =  await Firestore.instance.collection("users")
  //   .document(currentUid).collection("quizId").getDocuments();
  //
  //   for (var quiz in quizes.documents){
  //     print(quiz.data);
  //   }
  // }

  // getUserResultStream() async {
  //   authservice.getCurrentUser();
  //   final currentUid = authservice.loggedInUser.uid;
  //   await for (var snapshot in Firestore.instance.collection("users")
  //       .document(currentUid).collection("quizId").snapshots()){
  //     for (var quiz in snapshot.documents){
  //       print(quiz.data);
  //     }
  //   }
  // }

  getQuizezData(String quizId) async{
    print(quizId);
    return await Firestore.instance
        .collection("Quiz")
        .document(quizId)
        .collection("QNA")
        .getDocuments();
  }

  getUserInformation(String adminUserUid) async{
    return await Firestore.instance.collection("users").snapshots();
  }

  Future<String> getUserInfo(String adminId) async{
    return await Firestore.instance.collection("users").document(adminId).get().then((value){
      return value['name'];
    });
  }

  Future<DocumentReference> getUserDoc() async {

    final Firestore _firestore = Firestore.instance;
     DocumentReference ref = _firestore.collection('users').document('zeSovUKLA9hAuizY2X7MAI91pfq1');
     print(ref);
    return ref;
  }


}
