import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/user_controller.dart';
import 'package:quiz/models/created_quiz_model.dart';
import 'package:quiz/models/question_model.dart';
import 'package:quiz/models/quiz_model.dart';
import 'package:quiz/models/user.dart';
import 'package:quiz/views/addquestion.dart';
import 'package:quiz/views/home_page.dart';

class QuizServices{
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  Stream<List<QuizModel>> quizStream() {
    return _firestore
        .collection(QUIZ_COLLECTION)
        .orderBy(DATE_CREATED, descending: true)
        .snapshots()
        .map((QuerySnapshot query) {
      List<QuizModel> retVal = List();
      query.docs.forEach((element) {
          retVal.add(QuizModel.fromDocumentSnapshot(doc:element));
      });
      return retVal;
    });
  }

  addQuiz(Map map,List<Question> list,Map questionMap){
    DocumentReference _ref=_firestore.collection(USERS_COLLECTION)
        .doc(Get.find<UserController>().user.userId);
    _firestore.collection(QUIZ_COLLECTION).doc(map[QUIZ_ID]).set({
      QUIZ_IMAGE:map[QUIZ_IMAGE],
      QUIZ_TITLE:map[QUIZ_TITLE],
      QUIZ_DESCRIPTION:map[QUIZ_DESCRIPTION],
      QUIZ_CREATER_REFERENCE:_ref,
      QUESTION_LIST:questionMap[QUESTION_LIST],
      QUIZ_ID:map[QUIZ_ID],
      DATE_CREATED:DateTime.now().toString(),
    }).then((value)async{
      DocumentReference _qRef= _firestore.collection(QUIZ_COLLECTION).doc(map[QUIZ_ID]);
      for(int i=0; i<list.length; i++){
        Map<String, String> questionMap = {
          "question":list[i].question,
          "option1": list[i].option1,
          "option2":list[i]. option2,
          "option3":list[i]. option3,
          "option4":list[i]. option4,
          "correct_answer":list[i].correctAnswer
        };
        addQuestion(map[QUIZ_ID], questionMap);
      }
    await  _firestore.collection(USERS_COLLECTION).
      doc(Get.find<UserController>().user.userId)
      .collection(CREATED_QUIZ_COLLECTION).doc(map[QUIZ_ID]).set({
        QUIZ_REFERENCE:_qRef,
        DATE_CREATED:DateTime.now().toString(),
      });
      Get.off(HomePage());
    } );
    // .catchError((e)=>Get.snackbar("Error while creating quiz", e.toString()));
  }
  // updateQuestions(Map map,String id,{int check})async{
  //   await  _firestore.collection(QUIZ_COLLECTION)
  //       .doc(id)
  //       .update({
  //     QUESTION_LIST:map[QUESTION_LIST]
  //   }).then((value)async{
  //     if(check!=1){
  //       Get.snackbar("Success", "Quiz Updated");
  //       Get.off(HomePage());
  //     }
  //
  //   });
  // }
  Stream<List<CreatedQuizModel>> createdQuizStream(String uid) {
    return _firestore
        .collection(USERS_COLLECTION).doc(uid)
        .collection(CREATED_QUIZ_COLLECTION)
        .orderBy(DATE_CREATED, descending: true)
        .snapshots()
        .map((QuerySnapshot query) {
      List<CreatedQuizModel> retVal = List();
      query.docs.forEach((element) {
        retVal.add(CreatedQuizModel.fromDocumentSnapshot(doc:element));
      });
      return retVal;
    });
  }

   Future getAttemptedUsers(String id) async {
     print("id is");
     print(id);
    QuerySnapshot qs = await _firestore.collection(QUIZ_COLLECTION)
        .doc(id).collection(SOLVED_USERS)
     .orderBy(SCORE,descending: true)
        .get();
    return qs.docs;
  }

  deleteQuiz(String quizId)async{
  await  _firestore.collection(USERS_COLLECTION)
        .doc(Get.find<UserController>().user.userId)
        .collection(CREATED_QUIZ_COLLECTION).doc(quizId).delete();
   await _firestore.collection(QUIZ_COLLECTION).doc(quizId).delete()
        .then((value) => Get.snackbar("Success", "quiz deleted"))
    .catchError((e)=>Get.snackbar("error", e));
  }
  addQuestion(String quizId,Map map,{int id}){
    _firestore.collection(QUIZ_COLLECTION).doc(quizId)
        .collection(QUESTIONS).doc().set({
          QUESTION:map[QUESTION],
          OPTION1:map[OPTION1],
          OPTION2:map[OPTION2],
          OPTION3:map[OPTION3],
          OPTION4:map[OPTION4],
          CORRECT_ANSWER:map[CORRECT_ANSWER]
    });
    // if(id!=null){
    //   Get.back();
    //   // Get.back();
    // }
  }
  updateQuestion({String quizId, docId, Map map}){
    _firestore.collection(QUIZ_COLLECTION).doc(quizId)
        .collection(QUESTIONS).doc(docId).update({
      QUESTION:map[QUESTION],
      OPTION1:map[OPTION1],
      OPTION2:map[OPTION2],
      OPTION3:map[OPTION3],
      OPTION4:map[OPTION4],
      CORRECT_ANSWER:map[CORRECT_ANSWER]
    }).then((value) {
      Get.snackbar("Success", "question updated");
      Get.back();
      Get.back();
    }).catchError((e)=>Get.snackbar("error", e));
  }

  Future getQuizDocs(String id) async {
    print("id is");
    print(id);
    QuerySnapshot qs = await _firestore.collection(QUIZ_COLLECTION)
        .doc(id).collection(QUESTIONS)
        // .orderBy(SCORE,descending: true)
        .get();
    return qs.docs;
  }

}