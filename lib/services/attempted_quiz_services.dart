
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/models/attempted_quiz_model.dart';
import 'package:quiz/views/home_page.dart';

class AttemptedQuizServices{
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  Stream<List<AttemptedQuizModel>> quizStream(String uid) {
    return _firestore
          .collection(USERS_COLLECTION)
          .doc(uid)
        .collection(ATTEMPTED_QUIZ_COLLECTION)
        .orderBy(DATE_CREATED, descending: true)
        .snapshots()
        .map((QuerySnapshot query) {
      List<AttemptedQuizModel> retVal = List();
      query.docs.forEach((element) {
        retVal.add(AttemptedQuizModel.fromDocumentSnapshot(doc:element));
      });
      return retVal;
    });
  }

  addQuiz(Map map){
    DocumentReference _ref=_firestore.collection(QUIZ_COLLECTION)
        .doc(map[QUIZ_ID]);
    _firestore
        .collection(USERS_COLLECTION).doc(Get.find<FirebaseController>().auth.currentUser.uid)
        .collection(ATTEMPTED_QUIZ_COLLECTION).doc().set({
      QUIZ_ID:map[QUIZ_ID],
      QUIZ_NAME:map[QUIZ_NAME],
      SCORE:map[SCORE],
      TOTAL_MARKS:map[TOTAL_MARKS],
      CREATER_REFERENCE:map[CREATER_REFERENCE],
      DATE_CREATED:DateTime.now().toString(),
      QUIZ_REFERENCE:_ref
    });
  }
  updateQuestions(Map map,String id)async{
    await  _firestore.collection(QUIZ_COLLECTION)
        .doc(id)
        .update({
      QUESTION_LIST:map[QUESTION_LIST]

    }).then((value)async{
      Get.snackbar("Success", "Quiz Updated");
      Get.off(HomePage());

    }).catchError((onError)=>
        Get.snackbar("Error While Update Info ", onError.message));
  }
}