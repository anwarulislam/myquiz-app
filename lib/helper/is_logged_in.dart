


import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/views/home_page.dart';
import 'package:quiz/views/login_page.dart';
import 'package:quiz/views/welcome_page.dart';

class IsSignedIn extends GetWidget<FirebaseController> {

  @override
  Widget build(BuildContext context) {
    print("user is");
    print(Get.find<FirebaseController>().user);

    return Obx((){
      // Get.put(FirebaseController());
      return Get.find<FirebaseController>().user!=null ? HomePage() :HomePage();
    });
  }
}