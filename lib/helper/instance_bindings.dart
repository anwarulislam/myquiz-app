

import 'package:get/get.dart';
import 'package:quiz/controllers/firebase_controller.dart';


class InstanceBinding extends Bindings{
  @override
  void dependencies() {
    // Get.put(FirebaseController());
    Get.lazyPut<FirebaseController>(() => FirebaseController());
  }

}