


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/models/question_model.dart';
import 'package:quiz/models/quiz_model.dart';

class CreatedQuizModel{
  String id;
  String date;
  DocumentReference ref;
  List<Question> questions;
  String quizName;
  String image;
  String desc;
  String quizId;
  QuizModel quiz;
  CreatedQuizModel({this.id,this.date,this.ref,this.quizName,this.quiz,
  this.questions,this.quizId});
  CreatedQuizModel.fromDocumentSnapshot({DocumentSnapshot doc}) {
    var _questions=List<Question>();

    ref=doc[QUIZ_REFERENCE];
    ref.get().then((value) {
      if(value.data()[QUESTION_LIST]!=null && value.data()[QUESTION_LIST]!=""){
        for (var h in value.data()[QUESTION_LIST]) {
          _questions.add(Question.fromJson(h));
        }
      }
      quizName=value.data()[QUIZ_TITLE];
      image=value.data()[QUIZ_IMAGE];
     desc= value.data()[QUIZ_DESCRIPTION];
     quizId=value.data()[QUIZ_ID];
    });
    questions=_questions;

  }

}