

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quiz/constants/strings.dart';

class SolvedUsersModel{
  String userName;
  String image;
  int score;
  String position;
  String date;
  DocumentReference ref;
  SolvedUsersModel({this.userName,this.score,this.position,this.date,this.ref,
  this.image});
  SolvedUsersModel.fromDocumentSnapshot({DocumentSnapshot doc}) {
   date=doc.data()[DATE_CREATED];
   score=doc.data()[SCORE];
   ref=doc.data()[SOLVER_REFERENCE];
   ref.get().then((value){
     userName =value.data()[USER_NAME];
     image=value.data()[PROFILE_IMAGE];
   });

  }
}