import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';

class UserResult{
  String quizCreator;
  String quizTitle;
  String score;
  DocumentReference creatorUid;

  UserResult({
    @required this.quizCreator,
    @required this.quizTitle,
    @required this.score,
    @required this.creatorUid,

  });
}