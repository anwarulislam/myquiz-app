



import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quiz/constants/strings.dart';

class AttemptedQuizModel{
  DocumentReference quizRef ;
  int score;
  int totalMarks;
  String id;
  String quizName;
  String image;
  String description;
  DocumentReference ref;
  AttemptedQuizModel({this.quizRef,this.quizName,this.score,
  this.totalMarks,this.id,this.ref});
  AttemptedQuizModel.fromDocumentSnapshot({DocumentSnapshot doc}) {
    score=doc.data()[SCORE];
    totalMarks=doc.data()[TOTAL_MARKS];
    id=doc.id;
    quizName=doc.data()[QUIZ_NAME];
    ref=doc.data()[CREATER_REFERENCE];
    quizRef=doc.data()[QUIZ_REFERENCE];
    quizRef.get().then((value){
      // quizName=value.data()[QUIZ_TITLE];
      image=value.data()[QUIZ_IMAGE];
      description=value.data()[QUIZ_DESCRIPTION];
    });

  }
}