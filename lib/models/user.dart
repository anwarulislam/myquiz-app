


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quiz/constants/strings.dart';

class UserModel{
  String name;
  String userName;
  String email;
  String profileImage;
  String userId;
  String location;
  UserModel({this.name,this.userName,this.email,this.profileImage,this.userId,
  this.location});
  UserModel.fromDocumentSnapshot({DocumentSnapshot doc}) {
  name=doc.data()[NAME];
  userName=doc.data()[USER_NAME];
  email=doc.data()[EMAIL];
  profileImage=doc.data()[PROFILE_IMAGE];
  location=doc.data()[LOCATION];
  userId=doc.id;

  }
}
