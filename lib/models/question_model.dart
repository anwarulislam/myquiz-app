import 'package:quiz/constants/strings.dart';

class QuestionModel {
  String question;
  String option1;
  String option2;
  String option3;
  String option4;
  String correctOption;
  bool answered;
}

class Question {
   String question;
   String option1;
   String option2;
   String option3;
   String option4;
   String correctAnswer;

  Question({
    this.question,
    this.option1,
    this.option2,
    this.option3,
    this.option4,
    this.correctAnswer
  });
  factory Question.fromJson(Map<String, dynamic> json) {
    try {

      return Question(
        question: json[QUESTION],
        option1: json[OPTION1],
        option2: json[OPTION2],
        option3: json[OPTION3],
        option4: json[OPTION4],
        correctAnswer: json[CORRECT_ANSWER],
      );
    } catch (e) {
      print('Exception Created');
      return null;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      QUESTION:question,
      OPTION1:option1,
      OPTION2:option2,
      OPTION3:option3,
      OPTION4:option4,
      CORRECT_ANSWER:correctAnswer
    };
  }
}

// List<Question> questionList = [
//   Question(
//     question: "I'm the life of the party:",
//     option1: "Disagree",
//     option2: "Slightly Disagree",
//     option3: "Neutral",
//     option4: "Slightly Agree",
//   ),
//   Question(
//     question: "I feel little concern for others:",
//     option1: "Disagree",
//     option2: "Slightly Disagree",
//     option3: "Neutral",
//     option4: "Slightly Agree",
//   ),
//   Question(
//     question: "I'm always prepared:",
//     option1: "Disagree",
//     option2: "Slightly Disagree",
//     option3: "Neutral",
//     option4: "Slightly Agree",
//   ),
//   Question(
//     question: "I get stressed out easily:",
//     option1: "Disagree",
//     option2: "Slightly Disagree",
//     option3: "Neutral",
//     option4: "Slightly Agree",
//   ),
//   Question(
//     question: "I Have a rich vocabulary:",
//     option1: "Disagree",
//     option2: "Slightly Disagree",
//     option3: "Neutral",
//     option4: "Slightly Agree",
//   ),
//   Question(
//     question: "I don't talk a lot:",
//     option1: "Disagree",
//     option2: "Slightly Disagree",
//     option3: "Neutral",
//     option4: "Slightly Agree",
//   ),
// ];
