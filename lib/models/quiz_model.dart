

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/models/question_model.dart';



class QuizModel {
 String image;
 String title;
 String description;
 String id;
 List<Question> questions;
 DocumentReference createrRef;

 QuizModel({this.image,this.title,this.description,this.createrRef,this.id,
 this.questions});
 QuizModel.fromDocumentSnapshot({DocumentSnapshot doc}) {
  var _questions=List<Question>();
  if(doc.data()[QUESTION_LIST]!=null && doc.data()[QUESTION_LIST]!=""){
   for (var h in doc.data()[QUESTION_LIST]) {
    _questions.add(Question.fromJson(h));

   }
  }
   id=doc.id;
  image=doc.data()[QUIZ_IMAGE];
  title=doc.data()[QUIZ_TITLE];
  description=doc.data()[QUIZ_DESCRIPTION];
  createrRef=doc.data()[QUIZ_CREATER_REFERENCE];
  questions=_questions;

 }
 factory QuizModel.fromJson(Map<String, dynamic> json) {
  var _questions=List<Question>();
  try {
   if(json[QUESTION_LIST]!=null && json[QUESTION_LIST]!=""){
    for (var h in json[QUESTION_LIST]) {
     _questions.add(Question.fromJson(h));

    }
   }
   return QuizModel(
    title: json[QUIZ_TITLE]
   );
  } catch (e) {
   print('Exception Created');
   return null;
  }
 }
}

// List<Quiz> quizList = [
//   Quiz(
//     question: 12,
//     quizCreator: "Ali Hassan",
//     quizImage:
//         "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg",
//     quizTitle: "Nature Is Amazing",
//   ),
//   Quiz(
//     question: 12,
//     quizCreator: "Ali Hassan",
//     quizImage:
//         "https://cdn.pixabay.com/photo/2014/04/14/20/11/flowers-324175_1280.jpg",
//     quizTitle: "Nature Is Amazing",
//   ),
//   Quiz(
//     question: 12,
//     quizCreator: "Ali Hassan",
//     quizImage:
//         "https://cdn.pixabay.com/photo/2014/04/14/20/11/flowers-324175_1280.jpg",
//     quizTitle: "Nature Is Amazing",
//   ),
//   Quiz(
//     question: 12,
//     quizCreator: "Ali Hassan",
//     quizImage:
//         "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg",
//     quizTitle: "Nature Is Amazing",
//   )
// ];
