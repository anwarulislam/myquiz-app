

/*
*
*
* HERE IS ALL COLLECTIONS NAMES ARE DEFINED
* */

const String USERS_COLLECTION='Users';
const String QUIZ_COLLECTION='Quiz';
const String ATTEMPTED_QUIZ_COLLECTION='attempted_quiz';
const String SOLVED_USERS='solved_users';
const String CREATED_QUIZ_COLLECTION='created_quiz_collection';
const String QUESTIONS='questions';
const String CONSTANTS='constants';
/*
*
*
* HERE IS COMMON ATTRIBUTES NAMES ARE DEFINED
* */
const String DATE_CREATED='date_created';
const String COUNT='count';
/*
*
*
* HERE IS USERS COLLECTIONS NAMES ARE DEFINED
* */
const String USER_NAME="user_name";
const String NAME='name';
const String EMAIL='email';
const String PROFILE_IMAGE='profile_image';
const String LOCATION='location';
/*
*
*
* HERE IS QUIZ COLLECTIONS NAMES ARE DEFINED
* */

const String QUIZ_IMAGE='quiz_image';
const String QUIZ_TITLE='quiz_title';
const String QUIZ_DESCRIPTION='quiz_desc';
const String QUIZ_CREATER_REFERENCE='creater_ref';
const String QUIZ_ID='quiz_id';
/*
*
*
* HERE IS QUIZ COLLECTIONS NAMES ARE DEFINED
* */

const String QUESTION='question';
const String OPTION1='option1';
const String OPTION2='option2';
const String OPTION3='option3';
const String OPTION4='option4';
const String CORRECT_ANSWER='correct_answer';
const String QUESTION_LIST='questions_list';
/*
*
*
* HERE IS ATTEMPTED QUIZ COLLECTIONS NAMES ARE DEFINED
* */

const String CREATER_REFERENCE='creater_reference';
const String TOTAL_MARKS='total_marks';
const String SCORE='score';
const String QUIZ_NAME='quiz_name';
const String QUIZ_REFERENCE='quiz_ref';

/*
*
*
* HERE IS ATTEMPTED QUIZ USERS COLLECTIONS NAMES ARE DEFINED
* */
// const String POSITION='position';
const String SOLVER_REFERENCE='user_reference';

