import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/models/question_model.dart';
import 'package:quiz/services/database.dart';
import 'package:quiz/services/quiz_services.dart';
import 'package:quiz/sizeconfig.dart';
import 'package:quiz/widgets/widgets.dart';

class AddQuestion extends StatefulWidget {
  final Map quizMap;
  AddQuestion(this.quizMap);
  @override
  _AddQuestionState createState() => _AddQuestionState();
}

class _AddQuestionState extends State<AddQuestion> {
  List<Question> questionList=List<Question>();
  DatabaseService databaseService = new DatabaseService();
  final _formKey = GlobalKey<FormState>();

  bool _isLoading = false;
TextEditingController _questionCtr=TextEditingController();
  TextEditingController _option1Ctr=TextEditingController();
  TextEditingController _option2Ctr=TextEditingController();
  TextEditingController _option3Ctr=TextEditingController();
  TextEditingController _option4Ctr=TextEditingController();
  // String question = "", option1 = "", option2 = "", option3 = "", option4 = "";
String correctAnswer;
  uploadQuestionData() async {
    if (_formKey.currentState.validate()) {
      // setState(() {
      //   _isLoading = true;
      // });
   print("called ok");
      questionList.add(Question(
        question: _questionCtr.text,
        option1: _option1Ctr.text,
        option2:  _option2Ctr.text,
        option3:  _option3Ctr.text,
        option4:  _option4Ctr.text,
        correctAnswer: correctAnswer,
      ));
      Map<String,dynamic> map={
        QUESTION: _questionCtr.text,
        OPTION1: _option1Ctr.text,
        OPTION2:  _option2Ctr.text,
        OPTION3:  _option3Ctr.text,
        OPTION4:  _option4Ctr.text,
        CORRECT_ANSWER: correctAnswer,
      };
      // QuizServices().addQuestion(widget.quizMap[QUIZ_ID], map);
   Map<String,dynamic> questionMap={
     QUESTION_LIST:questionList.map((myContact) => myContact.toJson()).toList(),
   };
   // QuizServices().updateQuestions(questionMap,widget.quizMap[QUIZ_ID],check: 1);
      setState(() {
        _questionCtr.clear();
        _option1Ctr.clear();
        _option2Ctr.clear();
        _option3Ctr.clear();
        _option4Ctr.clear();
      });

      // Map<String, String> questionMap = {
      //   "question": question,
      //   "option1": option1,
      //   "option2": option2,
      //   "option3": option3,
      //   "option4": option4,
      // };
      // await databaseService
      //     .addQuestionData(questionMap, widget.quizId)
      //     .then((value) {
      //   setState(() {
      //     _isLoading = false;
      //   });
      // });
    }
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Add Questions"),
        backgroundColor: primaryColor,
        centerTitle: true,
        elevation: 0.0,
        iconTheme: IconThemeData(color: Colors.white),
        brightness: Brightness.light,
      ),
      body: Container(
              child: Form(
                key: _formKey,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        TextFormField(
                          onEditingComplete: () => node.nextFocus(),

                          controller: _questionCtr,
                          validator: (val) =>
                              val.isEmpty ? "Enter Question" : null,
                          decoration: InputDecoration(
                            hintText: "Question",
                          ),
                          onChanged: (val) {
                          },
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        _buildOption(
                            index: 0,
                            controller: _option1Ctr
                        ),
                        // TextFormField(
                        //   controller: _option1Ctr,
                        //   validator: (val) =>
                        //       val.isEmpty ? "Enter Option1" : null,
                        //   decoration: InputDecoration(
                        //     hintText: "Option1 (correct Answer)",
                        //   ),
                        //   onChanged: (val) {
                        //     setState(() {
                        //       correctAnswer=val;
                        //     });
                        //
                        //   },
                        // ),
                        SizedBox(
                          height: 6,
                        ),
                        _buildOption(
                            index: 1,
                            controller: _option2Ctr
                        ),
                        // TextFormField(
                        //   controller: _option2Ctr,
                        //   validator: (val) =>
                        //       val.isEmpty ? "Enter Option2" : null,
                        //   decoration: InputDecoration(
                        //     hintText: "Enter Option2",
                        //   ),
                        //   onChanged: (val) {
                        //
                        //   },
                        // ),
                        SizedBox(
                          height: 6,
                        ),
                        _buildOption(
                            index: 2,
                            controller: _option3Ctr
                        ),
                        // TextFormField(
                        //   controller: _option3Ctr,
                        //   validator: (val) =>
                        //       val.isEmpty ? "Enter Option3" : null,
                        //   decoration: InputDecoration(
                        //     hintText: "Enter Option 3",
                        //   ),
                        //   onChanged: (val)  GestureDetector(
      //           onTap: () {
      //             setState(() {
      //               selectedIndex = 0;
      //             });
      //           },
      //           child: Card(
      //             color: BOTTOM_APP_WHITE_COLOR,
      //             elevation: 3,
      //             child: Container(
      //               width: SizeConfig.widthMultiplier*12,
      //               height: SizeConfig.heightMultiplier*6.7,
      //               decoration: BoxDecoration(
      //                 boxShadow: [
      //                   BoxShadow(
      //                     color: TEXT_FIELD_COLOR,
      //                     spreadRadius: 1,
      //                     blurRadius: 1,
      //                     offset: Offset(0, 1), // changes position of shadow
      //                   ),
      //                 ],
      //               ),
      //               child:Column(
      //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //                 crossAxisAlignment: CrossAxisAlignment.center,
      //                 children: [
      //                   SizedBox(height: SizeConfig.heightMultiplier*1,),
      //                   Icon(
      //                     Icons.home_filled,color: PRIMARY_COLOR,size: SizeConfig.textMultiplier*3,
      //                   ),
      //                   selectedIndex==0? Spacer():SizedBox(),
      //                   selectedIndex ==0?Icon(
      //                     Icons.arrow_drop_up,color: DARK_CREAM_COLOR,size: 15,
      //                   ): Container(),
      //                 ],
      //               ),
      //             ),
      //           ),
      //         ), {
                        //
                        //   },
                        // ),
                        SizedBox(
                          height: 6,
                        ),
                        _buildOption(
                          index: 3,
                          controller: _option4Ctr
                        ),
                        // TextFormField(
                        //   controller: _option4Ctr,
                        //   validator: (val) =>
                        //       val.isEmpty ? "Enter Option4" : null,
                        //   decoration: InputDecoration(
                        //     hintText: "Enter Option4",
                        //   ),
                        //   onChanged: (val) {
                        //
                        //   },
                        // ),
                        // Spacer(),
                        SizedBox(height: SizeConfig.heightMultiplier*5,),
                        Text("Please select the correct option"),
                        SizedBox(height: SizeConfig.heightMultiplier*20,),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                print("quiz id is");

                                // print(widget.quizId);
                                print("list length is");
                                print(questionList.length);
                                // Navigator.pop(context);
                                if(questionList.length>=2){
                                  Map<String,dynamic> questionMap={
                                    QUESTION_LIST:questionList.map((myContact) => myContact.toJson()).toList(),
                                  };
                                  Get.snackbar("Quiz Created", "You have Created a new Quiz");
                                  QuizServices().addQuiz(widget.quizMap, questionList,questionMap);
                                  // QuizServices().updateQuestions(questionMap,widget.quizMap[QUIZ_ID]);
                                }else{
                                  Get.snackbar("Submission Error", "Each quiz must contain at least 2 questions");
                                }

                              },
                              child: mybutton(
                                  context: context,
                                  label: "Submit",
                                  buttonWidth:
                                      MediaQuery.of(context).size.width / 2 - 36),
                            ),
                            SizedBox(
                              width: 24,
                            ),
                            GestureDetector(
                              onTap: () {
                                print("correct option is");
                                print(correctOption);
                                print(correctAnswer);
                                uploadQuestionData();
                                setState(() {
                                  correctOption=0;
                                });
                                print("correct option is");
                              },
                              child: mybutton(
                                  context: context,
                                  label: "add question",
                                  buttonWidth:
                                      MediaQuery.of(context).size.width / 2 - 36),
                            )
                          ],
                        ),

                        SizedBox(
                          height: 60,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
  int correctOption=0;
  _buildOption({int index, TextEditingController controller}) {
    final node = FocusScope.of(context);

    return Row(
      children: [
        Expanded(
          child: TextFormField(
            onEditingComplete: () => node.nextFocus(),

            controller: controller,
            validator: (val) =>
            val.isEmpty ? "Enter Option ${index + 1}" : null,
            decoration: InputDecoration(
              hintText: "Enter Option ${index + 1}",
            ),
            onChanged: (val) {
              if(correctOption==index){
                print("this is called 0");
                setState(() {
                  correctAnswer=val;
                });
              }
              // options[index] = val;
            },
          ),
        ),
        Radio(
            activeColor: primaryColor,
            value: index == correctOption ? true : false,
            groupValue: true,
            onChanged: (val) {
              setState(() {
                correctOption = index;
                correctAnswer=controller.text;
              });
            })
      ],
    );
  }
}
