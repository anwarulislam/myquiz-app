import 'package:flutter/material.dart';
import 'package:quiz/widgets/widgets.dart';

class ViewResults extends StatefulWidget {

  final String title;
  final String quizid;
  final String userUid;
  final String creator;
  final String score;
  final String rank;

  ViewResults({
    @required this.title,
    @required this.quizid,
    @required this.userUid,
    @required this.creator,
    @required this.score,
    @required this.rank,
  });

  @override
  _ViewResultsState createState() => _ViewResultsState();
}

class _ViewResultsState extends State<ViewResults> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(

            ),
            child: Text("${widget.title}"),
          )
        ],
      ),
    );
  }

}