import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:quiz/constants.dart';
import 'package:get/get.dart';
import 'package:quiz/controllers/attempted_quiz_controller.dart';
import 'package:quiz/models/attempted_quiz_model.dart';
import 'package:quiz/models/created_quiz_model.dart';
import 'package:quiz/models/quiz_model.dart';
import 'package:quiz/services/quiz_services.dart';
import 'package:quiz/sizeconfig.dart';
import 'package:quiz/views/quiz_detail_page.dart';
import 'package:quiz/views/update_quiz_screen.dart';
import 'package:quiz/views/user_attempt_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class StatScreen extends StatefulWidget {
  @override
  _StatScreenState createState() => _StatScreenState();
}

class _StatScreenState extends State<StatScreen> {
  int selectedToggle = 0;
  //
  // List<QuizModel> quizes = [
  //   QuizModel(
  //     title: "Quiz Title",
  //     // questions: 12,
  //     // imgPath: "images/new.jpg",
  //   ),
  //   QuizModel(
  //     title: "Quiz Title",
  //     // questions: 12,
  //     // imgPath: "images/new.jpg",
  //   ),
  //   QuizModel(
  //     title: "Quiz Title",
  //     // questions: 12,
  //     // imgPath: "images/new.jpg",
  //   ),
  //   QuizModel(
  //     title: "Quiz Title",
  //     // questions: 12,
  //     // imgPath: "images/new.jpg",
  //   ),
  //   QuizModel(
  //     title: "Quiz Title",
  //     // questions: 12,
  //     // imgPath: "images/new.jpg",
  //   ),
  //   QuizModel(
  //     title: "Quiz Title",
  //     // questions: 12,
  //     // imgPath: "images/new.jpg",
  //   ),
  //   QuizModel(
  //     title: "Quiz Title",
  //     // questions: 12,
  //     // imgPath: "images/new.jpg",
  //   ),
  // ];

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
        BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: MediaQuery.of(context).size.height),
        designSize: Size(360, 690),
        orientation: Orientation.portrait);
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left:8.0,top: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                      onTap:(){
                        Get.back();
                      },
                      child: Icon(Icons.arrow_back))
                ],
              ),
            ),
            _statBar(),
            _toggle(),
            Expanded(
              child:selectedToggle==0?
              GetX<AttemptedQuizController>(
                builder: (controller) {
                  return controller.createdQuiz.length!=0? ListView.builder(
                      itemCount: controller.createdQuiz.length,
                      itemBuilder: (context, index) {
                        return _createdQuizTile(index, controller.createdQuiz[index]);
                      }):Center(
                    child: Text("No quiz found"),
                  );
                }
              ): GetX<AttemptedQuizController>(
                  builder: (controller) {
                    return controller.quiz.length!=0? ListView.builder(
                        itemCount: controller.quiz.length,
                        itemBuilder: (context, index) {
                          return _attemptedQuizTile(index, controller.quiz[index]);
                        }):Center(
                      child: Text("No quiz found"),
                    );
                  }
              ),
            ),
          ],
        ),
      ),
    );
  }

  _statBar() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      height: 80,
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [primaryColor, orangeLite]),
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Quiz Created",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: SizeConfig.textMultiplier*1.7,
                  fontWeight: FontWeight.w400),
            ),
            GetX<AttemptedQuizController>(
              builder: (controller) {
                return Text(
                  "${controller.createdQuiz.length??' '}",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize:  SizeConfig.textMultiplier*2.5,
                      fontWeight: FontWeight.w800),
                );
              }
            )
          ],
        ),
        VerticalDivider(
          color: Colors.grey[100],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Total Attempts",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: SizeConfig.textMultiplier*1.7,
                  fontWeight: FontWeight.w400),
            ),
            GetX<AttemptedQuizController>(
              builder: (controller) {
                return Text(
                  "${controller.quiz.length??' '}",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: SizeConfig.textMultiplier*2.5,
                      fontWeight: FontWeight.w800),
                );
              }
            )
          ],
        ),
      ]),
    );
  }

  _toggle() {
    return Container(
      height: 50,
      width: double.infinity,
      child: Row(
        children: [_toggleButton("CREATED", 0), _toggleButton("ATTEMPTED", 1)],
      ),
    );
  }

  _toggleButton(String label, int index) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          setState(() {
            selectedToggle = index;
          });
        },
        child: Container(
          color: selectedToggle == index ? primaryColor : Colors.white10,
          child: Center(
              child: Text(
                label,
                style: TextStyle(
                    letterSpacing: 0.8,
                    fontWeight: FontWeight.w600,
                    color: selectedToggle == index ? Colors.white : Colors.black),
              )),
        ),
      ),
    );
  }

  _createdQuizTile(int index, CreatedQuizModel quiz) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: GestureDetector(
        onTap: () {
          Get.to(AttemptUserScreen(
            quiz: quiz,
          ));
        },
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 6),
          height: 100,
          width: double.infinity,
          color: index % 2 == 0
              ? Color(0xFF6a492b).withOpacity(0.2)
              : Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            children: [
              CircleAvatar(
                radius: 30,
                backgroundImage: NetworkImage(quiz.image),
              ),
              const SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    quiz.quizName??' ',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    height: 32,
                    child: Text(
                      "${quiz.desc??' '}",
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  // Text("Total Score: ${quiz.score}")
                ],
              ),
              const Spacer(),
              Stack(children: [
                Positioned(
                  top: 0,
                  right: 0,
                  child: CircleAvatar(
                    radius: 6,
                    backgroundColor: primaryColor.withOpacity(0.1),
                  ),
                ),
                CircleAvatar(
                  radius: 24,
                  backgroundColor: primaryColor.withOpacity(0.05),
                ),
              ]),
            ],
          ),
        ),
      ),
      actions: <Widget>[
        IconSlideAction(
          caption: 'Update',
          color: primaryColor,
          icon: Icons.update,
          onTap: () {
            Get.to(UpdateQuizScreen(
              quiz: quiz,
            ));
          },
        ),
      ],
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () {
            QuizServices().deleteQuiz(quiz.quizId);
            setState(() {

            });
            setState(() {

            });
          },
        ),
      ],
    );
  }
  _attemptedQuizTile(int index, AttemptedQuizModel quiz) {
    return GestureDetector(
      onTap: () {
        Get.to(QuizDetailPage(
          quiz: quiz,
        ));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 6),
        height: 100,
        width: double.infinity,
        color: index % 2 == 0
            ? Color(0xFF6a492b).withOpacity(0.2)
            : Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          children: [
            CircleAvatar(
              radius: 30,
              backgroundImage: NetworkImage(quiz.image??' '),
            ),
            const SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  quiz.quizName??' ',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 4,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.6,
                  height: 32,
                  child: Text(
                    "${quiz.description}",
                  ),
                ),
                const SizedBox(
                  height: 4,
                ),
                Text("Total Question: ${quiz.totalMarks}")
              ],
            ),
            const Spacer(),
            Stack(children: [
              Positioned(
                top: 0,
                right: 0,
                child: CircleAvatar(
                  radius: 6,
                  backgroundColor: primaryColor.withOpacity(0.1),
                ),
              ),
              CircleAvatar(
                radius: 24,
                backgroundColor: primaryColor.withOpacity(0.05),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}

// class QuizModel {
//   final String title;
//   final int questions;
//   final String imgPath;
//
//   QuizModel({this.title, this.questions, this.imgPath});
// }
