// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:quiz/services/auth.dart';
// import 'package:quiz/services/database.dart';
// import 'package:quiz/views/create_quiz.dart';
// import 'package:quiz/views/play_quiz.dart';
// import 'package:quiz/widgets/widgets.dart';
// // import 'package:firebase_admob/firebase_admob.dart';
//
// const String testDevice = 'Mobile_id';
//
// class Home extends StatefulWidget {
//   @override
//   _HomeState createState() => _HomeState();
// }
//
// class _HomeState extends State<Home> {
//   String currentUid, userUid, quizTitleOne, quizTitle, quizId;
//   // AuthService authservice = new AuthService();
//
//   // List<Text> documents = [];
//
//   Stream quizStream;
//   Stream resultStream;
//   Stream userStream;
//   String adminUserUid;
//
//   DatabaseService databaseService = new DatabaseService();
//
//   // static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
//   //     testDevices: testDevice != null ? <String>[testDevice] : null,
//   //     nonPersonalizedAds: true,
//   //     keywords: <String>['Game', 'Mario']);
//
//   // BannerAd _bannerAd;
//
//   // BannerAd createBannerAd() {
//   //   return BannerAd(
//   //       adUnitId: 'ca-app-pub-7607609998503764/7230517944',
//   //       size: AdSize.banner,
//   //       targetingInfo: targetingInfo,
//   //       listener: (MobileAdEvent event) {
//   //         print("bannerAd $event");
//   //       });
//   // }
//
//   @override
//   void initState() {
//
//     // authservice.getCurrentUser();
//     databaseService.getQuizData().then((val) {
//       setState(() {
//         quizStream = val;
//       });
//     });
//
//     // resultList();
//
//     // FirebaseAdMob.instance.initialize(appId: 'ca-app-pub-7607609998503764~7792790574');
//     // _bannerAd = createBannerAd()..load()..show(anchorType: AnchorType.bottom, anchorOffset: 50.0);
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     // _bannerAd.dispose();
//     super.dispose();
//   }
//
//   Widget quizList() {
//     return Container(
//       margin: EdgeInsets.symmetric(horizontal: 24),
//       child: StreamBuilder(
//         stream: quizStream,
//         builder: (context, snapshot) {
//           return StreamBuilder(builder: (context, snapshot2) {
//             return snapshot.data == null
//                 ? Container()
//                 : ListView.builder(
//                     itemCount: snapshot.data.documents.length,
//                     itemBuilder: (context, index) {
//                       return QuizTile(
//                         imgUrl:
//                             snapshot.data.documents[index].data["quizImgUrl"],
//                         desc: snapshot.data.documents[index].data["quizDesc"],
//                         title: snapshot.data.documents[index].data["quizTitle"],
//                         quizid: snapshot.data.documents[index].data["quizId"],
//                         userUid: snapshot
//                             .data.documents[index].data["userInformation"],
//                       );
//                     });
//           });
//         },
//       ),
//     );
//   }
//
//   Widget resultFetch() {
//     // UserResult userResult = new UserResult();
//     // quizTitleOne = "fetch";
//     userUid = authservice.loggedInUser.uid;
//     print('result not fetch');
//     currentUid = authservice.loggedInUser.uid;
//     Stream<QuerySnapshot> courseDocStream = Firestore.instance
//         .collection("users")
//         .document(currentUid)
//         .collection("quizId")
//         .snapshots();
//
//     return Container(
//         child: StreamBuilder<QuerySnapshot>(
//       stream: courseDocStream,
//       builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
//         if (!snapshot.hasData) {
//           return Center(
//             child: CircularProgressIndicator(
//               backgroundColor: Colors.lightBlueAccent,
//             ),
//           );
//         }
//         final int messageCount = snapshot.data.documents.length;
//         return ListView.builder(
//             itemCount: messageCount,
//             itemBuilder: (_, int index) {
//               final DocumentSnapshot document = snapshot.data.documents[index];
//
//               // print(document['quizCreator']);
//               return ListTile(
//                 title: Text(document['quizTitle'] ?? '<No message retrieved>'),
//                 subtitle: Text('Message ${index + 1} of $messageCount'),
//               );
//             });
//
//         final quizResults = snapshot.data.documents;
//
//         return Column(
//           children: [
//             DataTable(
//               columns: <DataColumn>[
//                 DataColumn(label: Text('Quiz')),
//                 DataColumn(label: Text('Creator')),
//                 DataColumn(label: Text('Score')),
//                 DataColumn(label: Text('Rank')),
//               ],
//               rows: quizResults
//                   .map((e) => DataRow(cells: [
//                         DataCell(Text(e['quizTitle'])),
//                         DataCell(Text(e['quizCreator'])),
//                         // DataCell(Text(e[userUid])),
//                         DataCell(Text(e['score'])),
//                         DataCell(Text(e['rank'])),
//                       ]))
//                   .toList(),
//             ),
//           ],
//         );
//       },
//     ));
//   }
//
//   Widget resultList() {
//     // print(quizResultsWidget[0]);
//     print("answer");
//     return Column(
//       children: [
//         resultFetch(),
//         Container(
//           alignment: Alignment.topCenter,
//           child: DataTable(
//             columns: <DataColumn>[
//               DataColumn(label: Text('Quiz')),
//               DataColumn(label: Text('Creator')),
//               DataColumn(label: Text('Score')),
//               DataColumn(label: Text('Rank')),
//             ],
//             rows: <DataRow>[
//               DataRow(
//                 cells: <DataCell>[
//                   DataCell(Text('Sarah')),
//                   DataCell(Text('19')),
//                   // DataCell((quizResultsWidget[0])),
//                   DataCell(Text('Student')),
//                   DataCell(Text('Student')),
//                 ],
//               ),
//             ],
//           ),
//         ),
//         // quizResultsWidget.removeLast()
//       ],
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.grey[100],
//       appBar: AppBar(
//         title: Text("QuizNow"),
//         centerTitle: true,
//         backgroundColor: Colors.black,
//         elevation: 0.0,
//         brightness: Brightness.light,
//       ),
//       body: Container(child: quizList()),);
//   }
//
// }
//
// class QuizTile extends StatefulWidget {
//   final String imgUrl;
//   final String title;
//   final String desc;
//   final String quizid;
//   final DocumentReference userUid;
//   // final DocumentReference creatorUid;
//
//   QuizTile({
//     @required this.imgUrl,
//     @required this.title,
//     @required this.desc,
//     @required this.quizid,
//     @required this.userUid,
//     // @required this.creatorUid,
//   });
//
//   @override
//   _QuizTileState createState() => _QuizTileState();
// }
//
// class _QuizTileState extends State<QuizTile> {
//   String userName, creatorName;
//   DocumentReference creatorUid;
//
//   @override
//   void initState() {
//     widget.userUid.get().then((value) {
//       setState(() {
//         userName = value.data()['name'];
//       });
//     });
//
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         Navigator.push(context,
//             MaterialPageRoute(builder: (context) => PlayQuiz(widget.quizid)));
//       },
//       child: Container(
//         margin: EdgeInsets.only(top: 10),
//         height: 150,
//         child: Stack(
//           children: [
//             ClipRRect(
//                 borderRadius: BorderRadius.circular(8),
//                 child: Image.network(
//                   widget.imgUrl,
//                   width: MediaQuery.of(context).size.width - 48,
//                   fit: BoxFit.cover,
//                 )),
//             Container(
//               // color: Colors.black26,
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(8),
//                 color: Colors.black26,
//               ),
//               alignment: Alignment.center,
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Text(
//                     widget.title,
//                     style: TextStyle(
//                         color: Colors.white,
//                         fontSize: 17,
//                         fontWeight: FontWeight.w500),
//                   ),
//                   SizedBox(
//                     height: 6,
//                   ),
//                   Text(widget.desc,
//                       style: TextStyle(
//                           color: Colors.white,
//                           fontSize: 17,
//                           fontWeight: FontWeight.w500)),
//                   Row(mainAxisAlignment: MainAxisAlignment.center, children: [
//                     Text('Created By  ',
//                         style: TextStyle(
//                             color: Colors.white,
//                             fontSize: 17,
//                             fontWeight: FontWeight.w500)),
//                     Text(userName == null ? '  ' : userName,
//                         style: TextStyle(
//                             color: Colors.white,
//                             fontSize: 17,
//                             fontWeight: FontWeight.w500)),
//                   ]),
//                 ],
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
//
// }
