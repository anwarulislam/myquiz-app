import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/models/created_quiz_model.dart';
import 'package:quiz/services/quiz_services.dart';

class AttemptUserScreen extends StatefulWidget {
  final CreatedQuizModel quiz;
  AttemptUserScreen({this.quiz});
  @override
  _AttemptUserScreenState createState() => _AttemptUserScreenState();
}

class _AttemptUserScreenState extends State<AttemptUserScreen> {
  // List<User> users = [
  //   User(
  //     name: "UserName",
  //     profileImage: "images/new.jpg",
  //     percentage: 67,
  //   ),
  //   User(
  //     name: "UserName",
  //     profileImage: "images/new.jpg",
  //     percentage: 67,
  //   ),
  //   User(
  //     name: "UserName",
  //     profileImage: "images/new.jpg",
  //     percentage: 67,
  //   ),
  //   User(
  //     name: "UserName",
  //     profileImage: "images/new.jpg",
  //     percentage: 67,
  //   ),
  //   User(
  //     name: "UserName",
  //     profileImage: "images/new.jpg",
  //     percentage: 67,
  //   ),
  //   User(
  //     name: "UserName",
  //     profileImage: "images/new.jpg",
  //     percentage: 67,
  //   )
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Users"),
        centerTitle: true,
        backgroundColor: primaryColor,
        elevation: 0,
      ),
      body: Container(
        child: FutureBuilder(
          future: QuizServices().getAttemptedUsers(widget.quiz.quizId),
          builder: (context, snapshot) {
            if(snapshot.connectionState==ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(),
              );
            }else{
              return snapshot.data.length!=0? ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return UserTile(
                      index: index,
                      userRef: snapshot.data[index].data()[SOLVER_REFERENCE],
                      score: snapshot.data[index].data()[SCORE],
                      date: snapshot.data[index].data()[DATE_CREATED],
                    );
                  }):Center(
                child: Text("No user found"),
              );
            }

          }
        ),
      ),
    );
  }

}

class UserTile extends StatefulWidget {
 final int index;
 final int score;
 final DocumentReference userRef;
 final String date;
 UserTile({this.index,this.score,this.userRef,this.date});
  @override
  _UserTileState createState() => _UserTileState();
}

class _UserTileState extends State<UserTile> {
  String name;
  String image;
  @override
  void initState() {
    super.initState();
    widget.userRef.get().then((value) {
      setState(() {
        name=value.data()[NAME];
        image=value.data()[PROFILE_IMAGE];
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 0),
      // height: 60,
      width: double.infinity,
      color:widget. index % 2 == 0 ? Colors.grey[100] : Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            CircleAvatar(
              radius: 24,
              backgroundImage: NetworkImage(image??' '),
            ),
            const SizedBox(width: 8),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  name??' ',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 4,
                ),
                Text("Result Score: ${widget.score}"),
                const SizedBox(
                  height: 4,
                ),
                Text(
                    "Date: ${DateFormat('yyyy-MM-dd – kk:mm').format(DateTime.parse(widget.date))} "),
              ],
            ),
            const Spacer(),
            Stack(children: [
              // Positioned(
              //   top: 0,
              //   right: 0,
              //   child: CircleAvatar(
              //     radius: 6,
              //     backgroundColor: primaryColor.withOpacity(0.1),
              //   ),
              // ),
              CircleAvatar(
                radius: 24,
                child:widget.index==0 ||widget.index==1 || widget.index ==2?
                Text("${widget.index+1}",style: TextStyle(color: Colors.black),):Container(),
                backgroundColor: primaryColor.withOpacity(0.05),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}
