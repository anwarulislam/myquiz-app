import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/models/created_quiz_model.dart';
import 'package:quiz/models/question_model.dart';
import 'package:quiz/services/quiz_services.dart';
import 'package:quiz/views/update_question_screen.dart';

class UpdateQuizScreen extends StatefulWidget {
  final CreatedQuizModel quiz;
  UpdateQuizScreen({this.quiz});
  @override
  _UpdateQuizScreenState createState() => _UpdateQuizScreenState();
}

class _UpdateQuizScreenState extends State<UpdateQuizScreen> {
  // List<Question> questions = [
  //   Question(
  //     question: "Which city is the Largest City of Pakistan?",
  //     option1: "Lahore",
  //     option2: "Islamabad",
  //     option3: "Karachi",
  //     option4: "Lahore",
  //     correctOption: 3,
  //   ),
  //   Question(
  //     question: "Which city is the Largest City of Pakistan?",
  //     option1: "Lahore",
  //     option2: "Islamabad",
  //     option3: "Karachi",
  //     option4: "Lahore",
  //     correctOption: 3,
  //   ),
  //   Question(
  //     question: "Which city is the Largest City of Pakistan?",
  //     option1: "Lahore",
  //     option2: "Islamabad",
  //     option3: "Karachi",
  //     option4: "Lahore",
  //     correctOption: 3,
  //   ),
  //   Question(
  //     question: "Which city is the Largest City of Pakistan?",
  //     option1: "Lahore",
  //     option2: "Islamabad",
  //     option3: "Karachi",
  //     option4: "Lahore",
  //     correctOption: 3,
  //   ),
  //   Question(
  //     question: "Which city is the Largest City of Pakistan?",
  //     option1: "Lahore",
  //     option2: "Islamabad",
  //     option3: "Karachi",
  //     option4: "Lahore",
  //     correctOption: 3,
  //   ),
  // ];
  @override
  void initState() {
    super.initState();
    print("questions length is");
    print(widget.quiz.quizId);
    print(widget.quiz.questions.length);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Update Quiz"),
        centerTitle: true,
        backgroundColor: primaryColor,
        elevation: 0,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: primaryColor,
        child: Center(
          child: Icon(Icons.add),
        ),
        onPressed: () {

          Get.to(UpdateQuestion(
            pageId: 1,
            quizId: widget.quiz.quizId,
          )).then((value) {
            setState(() {

            });
          });
        },
      ),
      body: Container(
        child: FutureBuilder(
          future: QuizServices().getQuizDocs(widget.quiz.quizId),
          builder: (context, snapshot) {
            if(snapshot.connectionState==ConnectionState.waiting){
           return Center(
             child: CircularProgressIndicator(),
           );
            }else{
              return ListView.builder(
                itemCount:snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: GestureDetector(
                      onTap: (){

                        Get.to(UpdateQuestion(
                          docId: snapshot.data[index].id,
                          quizId: widget.quiz.quizId,
                          question: snapshot.data[index].data()[QUESTION],
                          option1: snapshot.data[index].data()[OPTION1],
                          option2: snapshot.data[index].data()[OPTION2],
                          option3: snapshot.data[index].data()[OPTION3],
                          option4: snapshot.data[index].data()[OPTION4],
                          correctAnswer: snapshot.data[index].data()[CORRECT_ANSWER],
                        )).then((value) {
                          setState(() {

                          });
                        });
                        // Get.to(UpdateQuestion(
                        //   question: widget.quiz.questions[index],
                        //   index: index,
                        //   list:widget.quiz,
                        //   // quizId: widget.quiz.quizId,
                        // ));
                      },
                      child: Container(
                        color:primaryColor,
                        height: 50,
                        child: Center(child: Text(snapshot.data[index].data()[QUESTION],style: TextStyle(
                          color: Colors.white
                        ),),),
                      ),
                    ),
                  );
                  QuestionTile(question: widget.quiz.questions[index], index: index,list:widget.quiz.questions,
                    quizId: widget.quiz.quizId,);
                },
              );
            }

          }
        ),
      ),
    );
  }
}

class QuestionTile extends StatefulWidget {
  final int index;
   Question question;
   List<Question> list;
   String quizId;
   QuestionTile({ this.index, this.question,this.list,this.quizId}) ;
  @override
  _QuestionTileState createState() => _QuestionTileState();
}
class _QuestionTileState extends State<QuestionTile> {
  int correctOption;
  TextEditingController _questionCtr=TextEditingController();
  TextEditingController _option1Ctr=TextEditingController();
  TextEditingController _option2Ctr=TextEditingController();
  TextEditingController _option3Ctr=TextEditingController();
  TextEditingController _option4Ctr=TextEditingController();
  @override
  void initState() {
    correctOption=widget.index;
    // correctOption = widget.question.correctOption;
    _questionCtr.text=widget.question.question;
    _option1Ctr.text=widget.question.option1;
    _option2Ctr.text=widget.question.option2;
    _option3Ctr.text=widget.question.option3;
    _option4Ctr.text=widget.question.option4;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);

    return Container(
      color: widget.index % 2 == 0 ? Colors.grey[100] : Colors.white,
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 2),
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Form(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              TextFormField(
                onEditingComplete: () => node.nextFocus(),

                controller: _questionCtr,
                // initialValue: widget.question.question,
              ),
              SizedBox(
                height: 6,
              ),
              _buildOption(
                optionNo: 1,
                option: widget.question.option1,
                controller: _option1Ctr

              ),
              SizedBox(
                height: 6,
              ),
              _buildOption(
                optionNo: 2,
                option: widget.question.option2,
                  controller: _option2Ctr
              ),
              SizedBox(
                height: 6,
              ),
              _buildOption(
                optionNo: 3,
                option: widget.question.option3,
                  controller: _option3Ctr
              ),
              SizedBox(
                height: 6,
              ),
              _buildOption(
                optionNo: 4,
                option: widget.question.option4,
                  controller: _option4Ctr
              ),
               RaisedButton(onPressed: (){
                 // Map<String,dynamic> questionMap={
                 //   QUESTION_LIST:questionList.map((myContact) => myContact.toJson()).toList(),
                 // };
                 // QuizServices().updateQuestions(questionMap,widget.quizId);
               },
                child: Text("update"),)
            ],
          ),
        ),
      ),
    );
  }
  _buildOption({int optionNo, String option,TextEditingController controller,int index}) {
    final node = FocusScope.of(context);

    return Row(
      children: [
        Expanded(
          child: TextFormField(
            onEditingComplete: () => node.nextFocus(),

    controller: controller,
            // initialValue: option,
            onChanged: (val) {

            },
            onFieldSubmitted:(value){
             widget.list[index].option1=value;
             // print("saved");
            },
          ),
        ),
        Radio(
            activeColor: primaryColor,
            value: optionNo == correctOption ? true : false,
            groupValue: true,
            onChanged: (val) {
              setState(() {
                correctOption = optionNo;
                setState(() {
                  widget.list[index].correctAnswer=controller.text;
                });

                // if(correctOption==1){
                //   widget.list[index].option1=controller.text;
                // }
                // if(correctOption==2){
                //   widget.list[index].option1=controller.text;
                // }
                // if(correctOption==3){
                //
                // }if(correctOption==4){
                //
                // }
              });
            })
      ],
    );
  }
}
// class Question {
//   String question;
//   String option1;
//   String option2;
//   String option3;
//   String option4;
//   int correctOption;
//   Question({
//     this.correctOption,
//     this.option1,
//     this.option2,
//     this.option3,
//     this.option4,
//     this.question,
//   });
// }
