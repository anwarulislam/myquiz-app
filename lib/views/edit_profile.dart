import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/controllers/user_controller.dart';
import 'package:quiz/services/database.dart';
import 'package:quiz/sizeconfig.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  Color defaultColor = Color(0xff3f9dff);
  Color whiteColor = Colors.white;

  TextEditingController _nameCtr = TextEditingController();
  TextEditingController _userName = TextEditingController();
  TextEditingController _locationCtr = TextEditingController();

  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  List<Map<String, dynamic>> listGender = [
    {
      "id": '0',
      "name": 'Male',
    },
    {
      "id": '1',
      "name": 'Female',
    }
  ];

  String SelectedValue;
  DateTime date = DateTime.now();
  var _image;

  void showDemoActionSheet({BuildContext context, Widget child}) {
    showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        setState(() {
          SelectedValue = value;
        });
      }
    });
  }

  Future getImageLibrary() async {
    var gallery =
        await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 700);
    setState(() {
      if (gallery != null) {
        // _image = File(gallery.path);
        DatabaseService().updateUserImage(File(gallery.path));
        setState(() {

        });
      } else {
        print('No image selected.');
      }
    });
  }

  Future cameraImage() async {
    var image =
        await ImagePicker.pickImage(source: ImageSource.camera, maxWidth: 700);
    setState(() {
      if (image != null) {
        // _image = File(image.path);
        DatabaseService().updateUserImage(File(image.path));
        setState(() {

        });
      } else {
        print('No image selected.');
      }
    });
  }

  selectCamera() {
    showDemoActionSheet(
      context: context,
      child: CupertinoActionSheet(
        title: const Text('Select Camera'),
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: const Text('Camera'),
            onPressed: () {
              Navigator.pop(context, 'Camera');
              cameraImage();
            },
          ),
          CupertinoActionSheetAction(
            child: const Text('Photo Library'),
            onPressed: () {
              Navigator.pop(context, 'Photo Library');
              getImageLibrary();
            },
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: const Text('Cancel'),
          isDefaultAction: true,
          onPressed: () {
            Navigator.pop(context, 'Cancel');
          },
        ),
      ),
    );
  }

  submit() {
    final FormState form = formKey.currentState;
    form.save();
  }
  UserController controller=Get.find();
@override
  void initState() {
    super.initState();
    setState(() {
      _nameCtr.text=Get.find<UserController>().user.name;
      _userName.text=Get.find<UserController>().user.userName;
      _locationCtr.text=Get.find<UserController>().user.location;
    });
    print("profile image is");
    print(controller.user.profileImage);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Get.back();
          },
          icon: Icon(Icons.arrow_back,color: Colors.black,),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: Row(
              children: [
                GestureDetector(onTap: (){
                  Get.find<FirebaseController>().signOut();
                },
                  child: Icon(
                    Icons.login,
                    color: Colors.black,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Text(
                    "Log Out",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w500),
                  ),
                ),
              ],
            ),
          )
        ],
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Create Account",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: SizeConfig.heightMultiplier * 3,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: SizeConfig.heightMultiplier * 3,
                ),
                Center(
                  child: Obx(()=> CircleAvatar(
                    backgroundColor: whiteColor,
                    radius: 61,
                    child: Stack(
                      children: [
                        CircleAvatar(
                          backgroundImage:NetworkImage(
                            controller.user.profileImage??' '
                          ),
                          radius: 60,
                        ),
                        GestureDetector(
                          onTap: (){
                            selectCamera();
                          },
                          child: CircleAvatar(
                              radius: 20,
                              backgroundColor: primaryColor,
                              child: Icon(Icons.edit,color: Colors.white,)),
                        ),
                      ],
                    ),
                  )),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: SizeConfig.heightMultiplier * 28),
                  CustomTextField(
                    // hintText: "usama Arif",
                    lable: "Name",
                    Controller: _nameCtr,
                  ),
                  CustomTextField(
                    lable: "Username",
                      Controller: _userName
                  ),
                  CustomTextField(
                    // hintText: "Vlad@gmail.com",
                    lable: "Location",
                      Controller: _locationCtr
                  ),
                  // GestureDetector(
                  //  onTap: (){
                  //
                  //  },
                  //   child: CustomTextField(
                  //     hintText: "11/08/1994",
                  //     lable: "Date of Birth",
                  //       Controller:dateTextEditingController
                  //   ),
                  // ),
                  SizedBox(height: SizeConfig.heightMultiplier * 3),
                  GestureDetector(
                    onTap: (){
                      _updateMap();
                    },
                    child: Container(
                      height: SizeConfig.heightMultiplier * 7,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              primaryColor,
                              primaryColor
                            ]),
                      ),
                      child: Center(
                        child: Text(
                          "Update",
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: SizeConfig.heightMultiplier * 3),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget CustomTextField({String hintText, int id, String lable,Controller}) {
    final node = FocusScope.of(context);

    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            lable,
            style: TextStyle(
                color: Colors.grey,
                fontSize: SizeConfig.textMultiplier * 1.8,
                fontWeight: FontWeight.bold),
          ),
          TextFormField(
            controller: Controller,
            onEditingComplete: () => node.nextFocus(),

            decoration: InputDecoration(
              hintText: hintText,
              hintStyle:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w500),

            ),
          ),
        ],
      ),
    );
  }

  _updateMap(){
   Map<String,dynamic> map={
     NAME:_nameCtr.text,
         USER_NAME:_userName.text,
    LOCATION:_locationCtr.text

   };
   DatabaseService().updateUser(map: map);
  }
}
