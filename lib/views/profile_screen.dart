import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/controllers/user_controller.dart';
import 'package:quiz/sizeconfig.dart';
import 'package:quiz/views/edit_profile.dart';

class ProfileScreen extends StatefulWidget {

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
 final Color whiteColor = Colors.white;

final UserController controller=Get.find();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Material(
                elevation: 0,
                color: primaryColor,
                child: Container(
                  height: SizeConfig.heightMultiplier * 35,
                  width: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                      gradient: LinearGradient(colors: [
                        primaryColor,
                        primaryColor,
                        primaryColor,
                  ])),
                ),
              ),
              Positioned(
                  left: SizeConfig.widthMultiplier*2,
                  top: SizeConfig.widthMultiplier*2,
                  child: GestureDetector(
                      onTap: (){
                        Get.back();
                      },
                      child: Icon(Icons.arrow_back,color: Colors.white,))),
              /*Container(
                height: SizeConfig.heightMultiplier * 35,
                width: MediaQuery.of(context).size.width,
                color: lite_black_color.withOpacity(.4),
              ),*/
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30, top: 50),
                child: Container(
                  height: SizeConfig.heightMultiplier * 36,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: whiteColor,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                            color: orangeLite.withOpacity(.5), blurRadius: 5)
                      ]),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: CircleAvatar(
                          backgroundColor: orangeLite,
                          radius: 63,
                          child: CircleAvatar(
                            backgroundColor: whiteColor,
                            radius: 61,
                            child:Obx(()=> CircleAvatar(
                              backgroundColor: orangeLite,
                              backgroundImage:
                                  NetworkImage(controller.user.profileImage??' '),
                              radius: 60,
                            )),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child:Obx(()=> Text("${controller.user.userName}",
                            style: TextStyle(
                                color: Color(0xFFF2c4e5e),
                                fontWeight: FontWeight.bold))),
                      ),
                     Obx(()=> Text("${controller.user.email}",
                          style: TextStyle(
                            color: Color(0xFFF2c4e5e),
                          ))),
                    ],
                  ),
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    height: SizeConfig.heightMultiplier * 45,
                  ),
                  profileTile(
                      lable: "Edit Account", icon: Icons.edit, ontap: () {
                        Get.to(EditProfileScreen()).then((value) {
                          setState(() {

                          });
                        });
                  }),
                  profileTile(
                      lable: "${controller.user.name}", icon: CupertinoIcons.person, ontap: () {}),
                  profileTile(
                      lable: "${controller.user.location}",
                      icon: CupertinoIcons.location,
                      ontap: () {}),
                  profileTile(
                      lable: "Log Out",
                      icon: CupertinoIcons.square_arrow_right,
                      ontap: () {
                       Get.find<FirebaseController>().signOut();
                      }),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget profileTile({lable, icon, ontap}) {
    return GestureDetector(
      onTap: ontap,
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 30),
                  child: CircleAvatar(
                    radius: SizeConfig.textMultiplier * 3.1,
                    backgroundColor: orangeLite.withOpacity(.5),
                    child: CircleAvatar(
                      backgroundColor: primaryColor,
                      radius: SizeConfig.textMultiplier * 3,
                      child: Center(
                        child: Icon(
                          icon,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Text(lable)
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20, top: 10),
              child: Divider(
                color: Colors.black.withOpacity(.5),
                thickness: .5,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
