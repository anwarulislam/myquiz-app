import 'package:flutter/material.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/models/attempted_quiz_model.dart';
import 'package:quiz/views/home_page.dart';
import 'package:get/get.dart';

class QuizDetailPage extends StatelessWidget {
  final AttemptedQuizModel quiz;
  QuizDetailPage({this.quiz});
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Quiz Result",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        width: size.width,
        height: size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(quiz.image??' '),
            const SizedBox(height: 20),
            Text(
              "${quiz.quizName}",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 26,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 8),
            SizedBox(
              width: size.width * 0.65,
              child: Text(
                "${quiz.description}",
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              "YOUR SCORE",
              style: TextStyle(
                  color: Colors.grey,
                  letterSpacing: 1.2,
                  fontSize: 14,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 4),
            Center(
              child: Text(
                "${quiz.score}/${quiz.totalMarks}",
                style: TextStyle(
                  color: primaryColor,
                  fontSize: 32,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            const SizedBox(height: 20),
            // GestureDetector(
            //     child: Container(
            //       height: 40,
            //       width: 120,
            //       decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(8),
            //           color: primaryColor),
            //       child: Center(
            //         child: Text(
            //           "Continue",
            //           style: TextStyle(color: Colors.white),
            //         ),
            //       ),
            //     ),
            //     onTap: () {
            //       Get.offAll(HomePage());
            //     }),
            const SizedBox(height: 30),
          ],
        ),
      ),
    );
  }
}
