import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/views/forgot_password_screen.dart';
import 'package:quiz/views/home_page.dart';
import 'package:quiz/views/sign_up.dart';
import 'package:quiz/widgets/bezire.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email, password;
  bool _isHidden  = true;

  final controller=Get.put(FirebaseController());
  void _toggle() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  // AuthService authservice = new AuthService();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
 final _formKey=GlobalKey<FormState>();
  bool _isLoading = false;

  signIn() async {
    print("called");
    // login(_emailController.text, _passwordController.text);
      controller.login(_emailController.text, _passwordController.text);

  }

  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
            ),
            Text('Back',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

  Widget _entryField(String title, TextEditingController controller,
      {bool isPassword = false}) {
    final node = FocusScope.of(context);

    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
              onEditingComplete: () => node.nextFocus(),

              controller: controller,
              obscureText: isPassword,

              validator: (value){
                if(value.isEmpty){
                  return "field is required";
                }else{
                  return null;
                }
              },
              decoration: InputDecoration(
                  /*hintText: 'Password',
                  suffix: InkWell(
                    onTap: _togglePasswordView,
                    child: Icon(
                      _isHidden
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                  ),*/
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true))
        ],
      ),
    );
  }
  Widget _passwordentryField(String title, TextEditingController controller,
      {bool isPassword = false}) {
    final node = FocusScope.of(context);

    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
              onEditingComplete: () => node.nextFocus(),

              controller: controller,
              obscureText: _isHidden,

              validator: (value){
                if(value.isEmpty){
                  return "field is required";
                }else{
                  return null;
                }
              },
              decoration: InputDecoration(
                  hintText: 'Password',
                  suffix: InkWell(
                    onTap: _togglePasswordView,
                    child: Icon(
                      _isHidden
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                  ),
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true))
        ],
      ),
    );
  }
  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  Widget _submitButton() {
    return GestureDetector(
      onTap: () {
        if(_formKey.currentState.validate()){
          if(GetUtils.isEmail(_emailController.text)){
            print("controller is");
            print(controller.isClosed);
            if(controller.isClosed){
              Get.snackbar("Error", "Controller is closed");
            }else{
              signIn();
            }
          }else{
            Get.snackbar("Invalid email", "Please enter valid email");
          }
        }
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Color(0xfffbb448), Color(0xfff7892b)])),
        child: Text(
          'Login',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  Widget _createAccountLabel() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SignUpPage()));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        padding: EdgeInsets.all(15),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Don\'t have an account ?',
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              width: 10,
            ),
            GestureDetector(
              onTap: (){
                Get.off(SignUpPage());
              },
              child: Text(
                'Register',
                style: TextStyle(
                    color: Color(0xfff79c4f),
                    fontSize: 13,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'Quiz',
          style: GoogleFonts.portLligatSans(
            textStyle: Theme.of(context).textTheme.headline4,
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Color(0xffe46b10),
          ),
          children: [
            TextSpan(
              text: 'Now',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
          ]),
    );
  }

  Widget _emailWidget() {
    return Column(
      children: <Widget>[
        _entryField("Email id", _emailController),
      ],

    );
  }
  // ignore: non_constant_identifier_names
  Widget __PasswordWidget() {
    return Column(
      children: <Widget>[
        _passwordentryField("Password", _passwordController, isPassword: true),
      ],

    );
  }

  @override
  void initState() {
    Get.put(FirebaseController());
    setState(() {

    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    final height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Container(

      height: height,
      child: Stack(
        children: <Widget>[
          Positioned(
              top: -height * .15,
              right: -MediaQuery.of(context).size.width * .4,
              child: BezierContainer()),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height:20),
                    _backButton(),
                    SizedBox(height: height * .2),
                    _title(),
                    SizedBox(height: 50),
                    _emailWidget(),
                    __PasswordWidget(),
                    SizedBox(height: 20),
                    _submitButton(),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        onTap: (){
                          Get.to(ForgotPasswordScreen());
                        },
                        child: Text('Forgot Password ?',
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500)),
                      ),
                    ),
                    // _divider(),
                    _createAccountLabel(),
                  ],
                ),
              ),
            ),
          ),
          // Positioned(top: 40, left: 0, child: _backButton()),
        ],
      ),
    ));
  }
  //  login(String email,String password) async
  // {
  //   // FirebaseAu auth = FirebaseAuth.instance;
  //   // Get.put(FirebaseController());
  //   await Get.find<FirebaseController>().auth.signInWithEmailAndPassword(email: email, password: password).then((value)async{
  //
  //     // if(Get.find<FirebaseController>().user!=null){
  //     //  Get.to(HomePage());
  //     // }else{
  //     // }
  //
  //
  //   });
  //
  // }
}
