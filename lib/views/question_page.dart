import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/question_controller.dart';
import 'package:quiz/models/question_model.dart';
import 'package:quiz/models/quiz_model.dart';
import 'package:quiz/services/attempted_quiz_services.dart';
import 'package:quiz/services/quiz_services.dart';
import 'package:quiz/services/solved_user_services.dart';
import 'package:quiz/views/result_page.dart';

class QuestionPage extends StatefulWidget {
 final QuizModel quiz;
  QuestionPage({this.quiz});
  @override
  _QuestionPageState createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  final QuestionController _controller = Get.put(QuestionController());
  int numbers=0;
  String selectedAnswer=' ';
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        // height: size.height,
        // width: size.width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                end: Alignment.bottomCenter,
                begin: Alignment.topCenter,
                colors: [
                  primaryColor,
                  primaryColor,
                  orangeLite,
                ])),
        child: FutureBuilder(
          future: QuizServices().getQuizDocs(widget.quiz.id),
          builder: (context, snapshot) {

              return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          //"${_controller.currentQuestion +1} - ${snapshot.data.length}",
                           "${_controller.currentQuestion + 1} - ${widget.quiz.questions.length}",
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                        // GestureDetector(
                        //   onTap: () {
                        //     Get.off(ResultPage());
                        //   },
                        //   child: Text("SKIP",
                        //       style:
                        //       TextStyle(color: Colors.white, fontSize: 16)),
                        // )
                      ],
                    ),
                    SizedBox(height: 30),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // height: SizeConfig.screenHeight * 0.2,

                          child: Text(
                            //snapshot.data[_controller.currentQuestion].data()[QUESTION],
                             widget.quiz.questions[_controller.currentQuestion].question,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 32,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        SizedBox(height: 30),
                        _buildOption(
                            //option: snapshot.data[_controller.currentQuestion].data()[OPTION1],
                             option: widget.quiz.questions[_controller.currentQuestion].option1,
                            optionNo: 1),
                        _buildOption(
                            //option: snapshot.data[_controller.currentQuestion].data()[OPTION2],
                             option: widget.quiz.questions[_controller.currentQuestion].option2,
                            optionNo: 2),
                        _buildOption(
                            //option: snapshot.data[_controller.currentQuestion].data()[OPTION3],
                             option: widget.quiz.questions[_controller.currentQuestion].option3,
                            optionNo: 3),
                        _buildOption(
                            //option:snapshot.data[_controller.currentQuestion].data()[OPTION4],
                             option: widget.quiz.questions[_controller.currentQuestion].option4,
                            optionNo: 4),
                      ],
                    ),
                    // QuestionBar(),
                    Spacer(),
                    //_nextButton(snapshot.data[_controller.currentQuestion].data()[CORRECT_ANSWER],widget.quiz.questions.length),
                    _nextButton(widget.quiz.questions[_controller.currentQuestion].correctAnswer,widget.quiz.questions.length),

                  ]);


          }
        ),
      )
    );
  }

  _nextButton(String answer,int length) {
    return GestureDetector(
      onTap: () {
        if (_controller.selectedOption == -1) {
          Get.snackbar(
              "Select Option", "select an option to continue to next question.",
              backgroundColor: Colors.white);
        } else {
          if (_controller.currentQuestion < length - 1) {
            if(selectedAnswer==answer){
              setState(() {
                numbers+=1;
              });

            }
            setState(() {

            });
            _controller.nextQuestion();
            _controller.setSelectedOption(-1);
          } else {
            if(selectedAnswer==answer){
              setState(() {
                numbers+=1;
              });

            }
            setState(() {

            });
            _sendAttemptedMap();
            Get.off(ResultPage(
              totalNumbers:widget.quiz.questions.length,
              givenNumbers: numbers,
            ));
          }
        }
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 65),
        height: 56,
        // width: SizeConfig.screenHeight,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Center(
          child: Text(
            _controller.currentQuestion == widget.quiz.questions.length - 1
                ? "Continue"
                : "Next",
            style: TextStyle(
              color: primaryColor,
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
  _buildOption({@required String option, @required int optionNo}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _controller.setSelectedOption(optionNo);
          selectedAnswer=option;
        });
      print("selected answer is");
      print(selectedAnswer);
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 6),
        height: 50,
        //  width: SizeConfig.screenHeight,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: _controller.selectedOption == optionNo
                ? Colors.white
                : primaryColor,
            border: Border.all(
              color: Colors.white,
            )),
        child: Row(children: [
          SizedBox(width: 16),
          CircleAvatar(
            radius: 10,
            backgroundColor: _controller.selectedOption == optionNo
                ? primaryColor
                : Colors.white,
            child: CircleAvatar(
              radius: 8,
              backgroundColor: _controller.selectedOption == optionNo
                  ? Colors.white
                  : primaryColor,
            ),
          ),
          SizedBox(width: 20),
          Text(
            option,
            style: TextStyle(
              color: _controller.selectedOption == optionNo
                  ? primaryColor
                  : Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w400,
            ),
          ),
        ]),
      ),
    );
  }
  _sendAttemptedMap(){
    Map<String,dynamic> map={
      QUIZ_ID:widget.quiz.id,
      QUIZ_NAME:widget.quiz.title,
      SCORE:numbers,
     TOTAL_MARKS:widget.quiz.questions.length,
      CREATER_REFERENCE:widget.quiz.createrRef,
    };
    AttemptedQuizServices().addQuiz(map);
    SolvedUserServices().addSolver(widget.quiz.id, numbers);
  }
}

// class QuestionBar extends StatelessWidget {
//   final QuestionController _controller = Get.find();
//
//   @override
//   Widget build(BuildContext context) {
//     return GetBuilder<QuestionController>(builder: (value) {
//       return Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Container(
//             // height: SizeConfig.screenHeight * 0.2,
//
//             child: Text(
//               questionList[_controller.currentQuestion].question,
//               style: TextStyle(
//                 color: Colors.white,
//                 fontSize: 32,
//                 fontWeight: FontWeight.w300,
//               ),
//             ),
//           ),
//           SizedBox(height: 30),
//           _buildOption(
//               option: questionList[_controller.currentQuestion].option1,
//               optionNo: 1),
//           _buildOption(
//               option: questionList[_controller.currentQuestion].option2,
//               optionNo: 2),
//           _buildOption(
//               option: questionList[_controller.currentQuestion].option3,
//               optionNo: 3),
//           _buildOption(
//               option: questionList[_controller.currentQuestion].option4,
//               optionNo: 4),
//         ],
//       );
//     });
//   }
//
//   _buildOption({@required String option, @required int optionNo}) {
//     return GestureDetector(
//       onTap: () {
//         _controller.setSelectedOption(optionNo);
//       },
//       child: Container(
//         margin: EdgeInsets.symmetric(vertical: 6),
//         height: 50,
//         //  width: SizeConfig.screenHeight,
//         decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(12),
//             color: _controller.selectedOption == optionNo
//                 ? Colors.white
//                 : primaryColor,
//             border: Border.all(
//               color: Colors.white,
//             )),
//         child: Row(children: [
//           SizedBox(width: 16),
//           CircleAvatar(
//             radius: 10,
//             backgroundColor: _controller.selectedOption == optionNo
//                 ? primaryColor
//                 : Colors.white,
//             child: CircleAvatar(
//               radius: 8,
//               backgroundColor: _controller.selectedOption == optionNo
//                   ? Colors.white
//                   : primaryColor,
//             ),
//           ),
//           SizedBox(width: 20),
//           Text(
//             option,
//             style: TextStyle(
//               color: _controller.selectedOption == optionNo
//                   ? primaryColor
//                   : Colors.white,
//               fontSize: 18,
//               fontWeight: FontWeight.w400,
//             ),
//           ),
//         ]),
//       ),
//     );
//   }
// }
