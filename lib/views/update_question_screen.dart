
import 'package:flutter/material.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/models/created_quiz_model.dart';
import 'package:quiz/models/question_model.dart';
import 'package:quiz/services/quiz_services.dart';
class UpdateQuestion extends StatefulWidget {
  String docId;
  String question;
  String option1;
  String option2;
  String option3;
  String option4;
  String correctAnswer;
  String quizId;
  int pageId;
  UpdateQuestion({this.docId,this.question,this.option1,this.option2,this.option3,
  this.option4,this.correctAnswer,this.quizId,this.pageId});
  // CreatedQuizModel list;
  //  Question question;
  //  int index;
   // UpdateQuestion({this.list,this.question,this.index});
  @override
  _UpdateQuestionState createState() => _UpdateQuestionState();
}

class _UpdateQuestionState extends State<UpdateQuestion> {
  int correctOption;
  TextEditingController _questionCtr=TextEditingController();
  TextEditingController _option1Ctr=TextEditingController();
  TextEditingController _option2Ctr=TextEditingController();
  TextEditingController _option3Ctr=TextEditingController();
  TextEditingController _option4Ctr=TextEditingController();
  String correctAnswer;
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    correctAnswer=widget.correctAnswer;
    // correctOption = widget.question.correctOption;
    _questionCtr.text=widget.question;
    _option1Ctr.text=widget.option1;
    _option2Ctr.text=widget.option2;
    _option3Ctr.text=widget.option3;
    _option4Ctr.text=widget.option4;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);

    return Scaffold(
      body: SafeArea(
        child: Container(
          color:  Colors.grey[100] ,
          width: double.infinity,
          margin: const EdgeInsets.symmetric(vertical: 2),
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Form(
            key: _formKey,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    TextFormField(
                      controller: _questionCtr,
                      onEditingComplete: () => node.nextFocus(),

                      decoration: InputDecoration(
                        hintText: "question"
                      ),
                      validator: (value){
                        if(value.isEmpty){
                          return "please enter question";
                        } else{
                          return null;
                        }
                      },
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    _buildOption(
                        optionNo: 1,
                        hint:"option 1",
                        controller: _option1Ctr

                    ),
                    SizedBox(
                      height: 6,
                    ),
                    _buildOption(
                        optionNo: 2,
                        hint: "option 2",
                        controller: _option2Ctr
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    _buildOption(
                        optionNo: 3,
                        hint: "option 3",
                        controller: _option3Ctr
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    _buildOption(
                        optionNo: 4,
                        hint: "option 4",
                        controller: _option4Ctr
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RaisedButton(onPressed: (){
                      Map<String,dynamic> map={
                        QUESTION: _questionCtr.text,
                        OPTION1: _option1Ctr.text,
                        OPTION2:  _option2Ctr.text,
                        OPTION3:  _option3Ctr.text,
                        OPTION4:  _option4Ctr.text,
                        CORRECT_ANSWER: correctAnswer,
                      };
                       if(_formKey.currentState.validate()){
                         if(widget.pageId==1){
                           QuizServices().addQuestion( widget.quizId, map,id: widget.pageId);

                         }else{
                           QuizServices().updateQuestion(quizId: widget.quizId,
                               docId: widget.docId,map: map);
                         }
                       }

                    },
                      color: primaryColor,
                      child: Text(widget.pageId==1?"add":"update",style: TextStyle(color: Colors.white),),)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  _buildOption({int optionNo, String hint,TextEditingController controller,int index}) {
    final node = FocusScope.of(context);

    return Row(
      children: [
        Expanded(
          child: TextFormField(
            onEditingComplete: () => node.nextFocus(),

            controller: controller,
            // initialValue: option,
            decoration: InputDecoration(
              hintText: hint
            ),
            validator: (value){
              if(value.isEmpty){
                return "please fill this field";
              } else{
                return null;
              }
            },
            onChanged: (val) {

            },
            onFieldSubmitted:(value){
              // widget.list.questions[index].option1=value;
              // print("saved");
            },
          ),
        ),
        Radio(
            activeColor: primaryColor,
            value:  correctAnswer==controller.text && controller.text.isNotEmpty ? true : false,
            groupValue: true,
            onChanged: (val) {
              setState(() {
                correctAnswer=controller.text;
              });
            })
      ],
    );
  }
}
