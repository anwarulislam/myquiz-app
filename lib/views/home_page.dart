import 'dart:developer';
import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/attempted_quiz_controller.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/controllers/quiz_controller.dart';
import 'package:quiz/controllers/user_controller.dart';
import 'package:quiz/models/quiz_model.dart';

import 'package:quiz/services/database.dart';
import 'package:quiz/services/dynamic_link_service.dart';
import 'package:quiz/sizeconfig.dart';
import 'package:quiz/views/create_quiz.dart';
import 'package:quiz/views/profile_screen.dart';
import 'package:quiz/views/question_page.dart';
import 'package:get/get.dart';
import 'package:quiz/views/splash_screen.dart';
import 'package:quiz/views/stat_screen.dart';
import 'package:quiz/views/welcome_page.dart';
import 'package:share/share.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

const String testDevice = 'Mobile_id';

class _HomePageState extends State<HomePage> {
  String currentUid, userUid, quizTitleOne, quizTitle, quizId;
  // AuthService authservice = new AuthService();

  // List<Text> documents = [];

  Stream quizStream;
  Stream resultStream;
  Stream userStream;
  String adminUserUid;
  TextEditingController _searchCtr = TextEditingController();
  DatabaseService databaseService = new DatabaseService();
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
      testDevices: testDevice != null ? <String>[testDevice] : null,
      nonPersonalizedAds: true,
      keywords: <String>['Game', 'Mario']);
  BannerAd _bannerAd;

  BannerAd createBannerAd() {
    return BannerAd(
        adUnitId: 'ca-app-pub-3940256099942544/6300978111',
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("bannerAd $event");
        });
  }

  getUser() async {
    // setState(() async{
    Get.put(UserController()).user = await DatabaseService()
        .getUser(Get.find<FirebaseController>().auth.currentUser.uid);
    // });

    // DatabaseService().updateStatus(value:true);
    print("new user id is");
    print(Get.find<UserController>().user.userId);

    Get.put(AttemptedQuizController())
        .createQuiz(Get.find<UserController>().user.userId);
    Get.put(AttemptedQuizController())
        .attemptQuiz(Get.find<UserController>().user.userId);

    setState(() {});
  }

  // AttemptedQuizController takenQuiz=Get.find();
  @override
  void initState() {
    if (Get.find<FirebaseController>().user != null) {
      Get.put(UserController());
    }

    FirebaseAdMob.instance
        .initialize(appId: 'ca-app-pub-3940256099942544~3347511713');
    _bannerAd = createBannerAd()
      ..load()
      ..show();
    getUser();
    Get.put(QuizController());
    Get.put(AttemptedQuizController());

    setState(() {});
// Get.offAll(SplashScreen());

    super.initState();
  }

  final statController = Get.put(AttemptedQuizController());
  @override
  void dispose() {
    _bannerAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 50.0),
        child: FloatingActionButton(
          backgroundColor: primaryColor,
          child: Center(
            child: Icon(Icons.add),
          ),
          onPressed: () {
            if (Get.find<FirebaseController>().user != null) {
              FirebaseFirestore.instance
                  .collection(CONSTANTS)
                  .doc("1")
                  .get()
                  .then((value) {
                Get.to(CreateQuiz(
                  count: value.data()[COUNT],
                ));
              });
            } else {
              Get.to(WelcomePage());
            }
          },
        ),
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          width: size.width,
          height: size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Get.find<FirebaseController>().user != null
                  ? Obx(() => _topBar())
                  : Container(),
              Get.find<FirebaseController>().user != null
                  ? Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Admin Panel:",
                            style: TextStyle(
                              color: primaryColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              Get.to(StatScreen());
                            },
                            icon: Icon(Icons.arrow_forward_ios),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              Get.find<FirebaseController>().user != null
                  ? _statBar()
                  : Container(),
              _entryField(),
              Container(
                padding: EdgeInsets.only(
                  top: 8,
                ),
                child: Text(
                  "Take Quiz",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Expanded(
                child: GetX<QuizController>(builder: (controller) {
                  return controller.quiz != null
                      ? ListView.builder(
                          itemCount: controller.quiz.length,
                          itemBuilder: (context, index) {
                            if (controller.isLoading.value) {
                              return Center(child: CircularProgressIndicator());
                            } else {
                              return controller.quiz.length != 0
                                  ? controller.quiz[index].title
                                          .toUpperCase()
                                          .contains(
                                              _searchCtr.text.toUpperCase())
                                      ? QuizTile(
                                          quiz: controller.quiz[index],
                                        )
                                      : Container()
                                  : Center(child: Text("No Quiz Available"));
                            }
                          })
                      : Container();
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _entryField() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Text(
          //   title,
          //   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          // ),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: _searchCtr,
            decoration: InputDecoration(
                hintText: "Search Quiz",
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true),
            onChanged: (val) {
              setState(() {});
            },
          )
        ],
      ),
    );
  }

  _topBar() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hi ${Get.find<UserController>().user.name ?? ' '}",
                  style: TextStyle(
                      color: primaryColor,
                      fontSize: 22,
                      fontWeight: FontWeight.w700),
                ),
                Text(
                  "Expand your knowledge!",
                  style: TextStyle(
                      color: Colors.grey[800],
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                )
              ],
            ),
            GestureDetector(
              onTap: () {
                Get.to(ProfileScreen());
              },
              child: CircleAvatar(
                radius: 26,
                backgroundColor: primaryColor,
                backgroundImage: NetworkImage(
                    Get.find<UserController>().user.profileImage ?? " "),
              ),
            ),
          ],
        ));
  }

  _statBar() {
    double perc;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 0),
      height: 80,
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [primaryColor, orangeLite]),
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Quiz Taken",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: SizeConfig.textMultiplier * 2,
                  fontWeight: FontWeight.w400),
            ),
            Obx(() => Text(
                  "${statController.quiz?.length ?? ' '}",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: SizeConfig.textMultiplier * 2.5,
                      fontWeight: FontWeight.w600),
                ))
            // GetX<AttemptedQuizController>(
            //
            //   builder: (controller) {
            //     return Text(
            //       "${controller.quiz?.length??' '}",
            //       style: TextStyle(
            //           color: Colors.white,
            //           fontSize:  SizeConfig.textMultiplier*2.5,
            //           fontWeight: FontWeight.w600),
            //     );
            //   }
            // )
          ],
        ),
        VerticalDivider(
          color: Colors.grey[100],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Result",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: SizeConfig.textMultiplier * 2,
                  fontWeight: FontWeight.w400),
            ),
            GetX<AttemptedQuizController>(builder: (controller) {
              if (controller.percentage.toString() == 'NaN') {
                perc = 0;
              } else {
                perc = controller.percentage;
              }
              if (perc > 100) {
                perc = 100;
              }
              return Text(
                //"${controller.percentage.toDouble().toStringAsFixed(2)=="NaN"?'0.0':controller.percentage!=null?  controller.percentage.toDouble().toStringAsFixed(2):' '} %",
                "${perc.toStringAsFixed(2)}%",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: SizeConfig.textMultiplier * 2.5,
                    fontWeight: FontWeight.w600),
              );
            })
            // Text(
            //   "67%",
            //   style: TextStyle(
            //       color: Colors.white,
            //       fontSize:  SizeConfig.textMultiplier*2.5,
            //       fontWeight: FontWeight.w600),
            // )
          ],
        ),
      ]),
    );
  }
}

class QuizTile extends StatefulWidget {
  final QuizModel quiz;

  QuizTile({this.quiz});

  @override
  _QuizTileState createState() => _QuizTileState();
}

class _QuizTileState extends State<QuizTile> {
  String name;
  @override
  void initState() {
    widget.quiz.createrRef.get().then((value) {
      setState(() {
        name = value.data()[NAME];
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (Get.find<FirebaseController>().user != null) {
          if (widget.quiz.questions.length == 0) {
            Get.snackbar("oops", "No Questions added yet");
          } else {
            Get.to(QuestionPage(
              quiz: widget.quiz,
            ));
          }
        } else {
          Get.to(WelcomePage());
        }
      },
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        secondaryActions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconSlideAction(
              // caption: 'More',
              color: primaryColor,
              icon: CupertinoIcons.share,

              onTap: () {
                DynamicLinkService()
                    .createFirstPostLink(widget.quiz.id)
                    .then((value) {
                  log(widget.quiz.id);
                  Share.share(
                      "https://play.google.com/store/apps/details?id=com.wise.quiz&hl=en&gl=US");
                });
              },
            ),
          ),
        ],
        child: Container(
          margin: EdgeInsets.only(top: 8),
          height: 120,
          decoration: BoxDecoration(
              boxShadow: [BoxShadow(color: Colors.black26)],
              color: Colors.white,
              borderRadius: BorderRadius.circular(12)),
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Image.network(
                      widget.quiz.image,
                      fit: BoxFit.cover,
                    )),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                top: 0,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      gradient: LinearGradient(
                          begin: Alignment.center,
                          end: Alignment.bottomCenter,
                          colors: [
                            primaryColor.withOpacity(0.1),
                            primaryColor.withOpacity(0.3),
                            primaryColor.withOpacity(0.5),
                            primaryColor.withOpacity(0.6),
                            primaryColor.withOpacity(0.7),
                            primaryColor.withOpacity(0.8),
                          ])),
                ),
              ),
              Positioned(
                bottom: 8,
                left: 12,
                right: 4,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.quiz.title ?? ' ',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                      Text(
                        "Total Question: ${widget.quiz.questions.length}  -  Created By: ${name ?? ' '}",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
