import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:quiz/constants.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/user_controller.dart';
import 'package:quiz/services/database.dart';
import 'package:quiz/services/quiz_services.dart';
import 'package:quiz/sizeconfig.dart';
import 'package:quiz/views/addquestion.dart';
import 'package:quiz/widgets/widgets.dart';
import 'package:random_string/random_string.dart';
import 'package:quiz/services/auth.dart';

class CreateQuiz extends StatefulWidget {
 final int count;
  CreateQuiz({this.count});
  @override
  _CreateQuizState createState() => _CreateQuizState();
}

class _CreateQuizState extends State<CreateQuiz> {
  final _formKey = GlobalKey<FormState>();
  String  quizTitle, quizDescription, quizId, uid, userUid,quizImage;
  DatabaseService databaseService = new DatabaseService();
  String quizImageUrl='https://firebasestorage.googleapis.com/v0/b/quiz-d78e5.appspot.com/o/quiz.png?alt=media&token=764ff3c2-d499-495f-b333-02bb3a9fcee0';
  String quiz3ImageUrl='https://firebasestorage.googleapis.com/v0/b/quiz-d78e5.appspot.com/o/quiz2.png?alt=media&token=fdb7b277-45f2-4978-8e31-d59c064c1ff3';
  String quiz2ImageUrl='https://firebasestorage.googleapis.com/v0/b/quiz-d78e5.appspot.com/o/quiz3.png?alt=media&token=e7677d0b-b453-4aa9-b087-97e08710fb9d';
  // AuthService authservice = new AuthService();
final user=Get.put(UserController());
  void initState() {
     FirebaseFirestore.instance.collection(CONSTANTS)
     .doc("1").update({
       COUNT:widget.count+1
     });
    super.initState();
  }

  bool _isLoading = false;

  createQuizOnline() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
      quizId = randomAlpha(25);
      Map<dynamic, dynamic> quizMap = {
        QUIZ_ID: quizId,
        QUIZ_IMAGE: quizImage==null?widget.count%2==0?quizImageUrl:quiz2ImageUrl:quizImage,
        QUIZ_TITLE: quizTitle,
        QUIZ_DESCRIPTION: quizDescription,
      };
      // QuizServices().addQuiz(quizMap);
      Get.to(AddQuestion(quizMap));
      setState(() {
        _isLoading = false;
      });

    }
  }
  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("QuizNow"),
        backgroundColor: primaryColor,
        centerTitle: true,
        elevation: 0.0,
        iconTheme: IconThemeData(color: Colors.white),
        brightness: Brightness.light,
      ),
      body: _isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Container(
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Column(
                      children: [
                        SizedBox(height: SizeConfig.heightMultiplier*2,),
                        Stack(
                          children: [
                            Container(
                              height: SizeConfig.heightMultiplier*20,
                              width: SizeConfig.widthMultiplier*100,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image:
                                quizImage!=null?  NetworkImage(
                                   quizImage??' '
                                  ):NetworkImage(
                                    widget.count%2==0?quizImageUrl??' ':quiz2ImageUrl??' '
                                )
                                )
                              ),
                            ),
                            Positioned(
                              right: SizeConfig.widthMultiplier*16,
                              bottom: 0,
                              child: GestureDetector(
                                onTap:(){
                                  selectCamera();
                                },
                                child: CircleAvatar(
                                  radius: 20,
                                  backgroundColor: primaryColor,
                                  child: Icon(Icons.edit,color: Colors.white,),
                                ),
                              ),
                            )
                          ],
                        ),
                        // TextFormField(
                        //   validator: (val) =>
                        //       val.isEmpty ? "Quiz image url" : null,
                        //   decoration: InputDecoration(
                        //     hintText: "Quiz Image URL",
                        //   ),
                        //   onChanged: (val) {
                        //     quizImageUrl = val;
                        //   },
                        // ),
                        SizedBox(
                          height: 6,
                        ),
                        TextFormField(
                          onEditingComplete: () => node.nextFocus(),

                          validator: (val) => val.isEmpty ? "Quiz Title" : null,
                          decoration: InputDecoration(
                            hintText: "Quiz Title",
                          ),
                          onChanged: (val) {
                            quizTitle = val;
                          },
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        TextFormField(
                          onEditingComplete: () => node.nextFocus(),

                          validator: (val) =>
                              val.isEmpty ? "Quiz Description " : null,
                          decoration: InputDecoration(
                            hintText: "Quiz Description",
                          ),
                          onChanged: (val) {
                            quizDescription = val;
                          },
                        ),
                        // Spacer(),
                        SizedBox(
                          height: SizeConfig.heightMultiplier*25,
                        ),
                        GestureDetector(
                            onTap: () {
                              createQuizOnline();
                            },
                            child:
                                mybutton(context: context, label: "Create Quiz")),



      SizedBox(
                          height: 60,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );

  }
  var _image;

  Future getImageLibrary() async {
    var pickedFile =
    await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 700);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        addQuizImage(File(pickedFile.path));
        // _pickSaveImage();
      }
    });
  }

  Future cameraImage() async {
    var pickedFile =
    await ImagePicker.pickImage(source: ImageSource.camera, maxWidth: 700);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        addQuizImage(File(pickedFile.path));
        // _pickSaveImage();
      }
    });
  }
  String lastSelectedValue;
  void showDemoActionSheet({BuildContext context, Widget child}) {
    showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        setState(() {
          lastSelectedValue = value;
        });
      }
    });
  }

  selectCamera() {
    showDemoActionSheet(
      context: context,
      child: CupertinoActionSheet(
          title: const Text("Select Picture"),
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: const Text('Camera'),
              onPressed: () {
                Navigator.pop(context, 'Camera');
                cameraImage();
              },
            ),
            CupertinoActionSheetAction(
              child: const Text('Gallery'),
              onPressed: () {
                Navigator.pop(context, 'Photo Library');
                getImageLibrary();
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Cancel'), //Zrušiť
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            },
          )),
    );
  }
  addQuizImage( image)async{
    var snapshot  =await FirebaseStorage.instance
        .ref()
        .child('quiz_images/${DateTime.now().toString() }myimage.jpg')
        .putFile(image);

    await snapshot.ref.getDownloadURL().then((value) {
      setState(() {
        quizImage=value;
      });

    });
  }
}
