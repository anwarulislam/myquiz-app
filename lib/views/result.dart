import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:quiz/services/auth.dart';
import 'package:quiz/services/database.dart';
import 'package:quiz/widgets/widgets.dart';

class Results extends StatefulWidget {
  final int correct, incorrect, total;
  final String quizId;
  Results(
      {@required this.correct,
      @required this.incorrect,
      @required this.total,
      @required this.quizId});

  @override
  _ResultsState createState() => _ResultsState();
}

class _ResultsState extends State<Results> {
  String userUid;

  DatabaseService databaseService = new DatabaseService();

  // AuthService authservice = new AuthService();

  void initState() {
    // TODO: implement initState
    // authservice.getCurrentUser();
    super.initState();
  }

  uploadResultData() async {
    // print(authservice.loggedInUser.uid);
    // userUid = authservice.loggedInUser.uid;
    print('user information from create $userUid');
    print(widget.correct.toString());
    String score = widget.correct.toString();
    String quizId = widget.quizId.toString();
    // Map<String, String> resultMap = {
    //   "userInformation": '/users/$userUid',
    //   "total": score,
    //   "quizID": quizId
    // };
    // await databaseService.addUserResult(resultMap, widget.quizId).then((value) {
    //   print("result state");
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/new.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("${widget.correct}/ ${widget.total}",
                  style: TextStyle(
                    fontSize: 25,
                  )),
              SizedBox(
                height: 8,
              ),
              Text(
                "You answered ${widget.correct} answers correctly and ${widget.incorrect} answers incorrectly",
                style: TextStyle(fontSize: 15, color: Colors.grey),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 14,
              ),
              GestureDetector(
                  onTap: () {
                    uploadResultData();
                    Navigator.pop(context);
                  },
                  child: mybutton(
                      context: context,
                      label: "Go to Home",
                      buttonWidth: MediaQuery.of(context).size.width / 2))
            ],
          ),
        ),
      ),
    );
  }
}
