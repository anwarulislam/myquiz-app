
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/helper/instance_bindings.dart';
import 'package:quiz/helper/is_logged_in.dart';
import 'package:quiz/sizeconfig.dart';
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    InstanceBinding();
    Get.put(FirebaseController());
    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
    //   return IsSignedIn();
    // }));
    Timer(Duration(seconds: 2), () {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
        return IsSignedIn();
      }));
    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:Container(
          height: SizeConfig.heightMultiplier*150,
          width: SizeConfig.widthMultiplier*300,
          child:Image.asset("images/splash.png")
        )
      ),
    );
  }
}
