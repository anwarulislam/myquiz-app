

import 'package:get/get.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/models/attempted_quiz_model.dart';
import 'package:quiz/models/created_quiz_model.dart';
import 'package:quiz/models/quiz_model.dart';
import 'package:quiz/services/attempted_quiz_services.dart';
import 'package:quiz/services/quiz_services.dart';


class AttemptedQuizController extends GetxController {
  var isLoading=true.obs;
  Rx<List<AttemptedQuizModel>> quizList = Rx<List<AttemptedQuizModel>>();
  List<AttemptedQuizModel> get quiz => quizList.value;
  Rx<List<CreatedQuizModel>> createdList = Rx<List<CreatedQuizModel>>();
  List<CreatedQuizModel> get createdQuiz => createdList.value;
  double get totalMarks => quizList.value.fold(0, (sum, item) => sum + item.totalMarks);
  double get obtainMarks => quizList.value.fold(0, (sum, item) => sum + item.score);
  double get percentage => (obtainMarks/totalMarks)*100;

  var lengthCount=0.obs;
  @override
  void onInit() {

    // String uid = Get.find<FirebaseController>().auth.currentUser.uid;
    // print("current user uid is");
    // print(uid);


    isLoading(false);
    super.onInit();

  }
  void clear() {
    quizList.value.clear();
    createdList.value.clear();
  }
  createQuiz(String uid){
    createdList
        .bindStream(QuizServices().createdQuizStream(uid));
  }
  attemptQuiz(String uid){
    quizList
        .bindStream(AttemptedQuizServices().quizStream(uid));
  }
}