

import 'package:get/get.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/models/quiz_model.dart';
import 'package:quiz/models/solved_users_model.dart';
import 'package:quiz/services/quiz_services.dart';
import 'package:quiz/services/solved_user_services.dart';


class SolvedUserController extends GetxController {
  var isLoading=true.obs;
  Rx<List<SolvedUsersModel>> solversList = Rx<List<SolvedUsersModel>>();
  List<SolvedUsersModel> get solvers => solversList.value;
  var lengthCount=0.obs;
  @override
  void onInit() {
    isLoading(true);
    String uid = Get.find<FirebaseController>().auth.currentUser.uid;
    // solversList
    //     .bindStream(SolvedUserServices().solverSteam(uid));
    isLoading(false);
    super.onInit();

  }
}