import 'package:quiz/models/question_model.dart';
import 'package:get/get.dart';

class QuestionController extends GetxController {
  int currentQuestion = 0;
  int selectedOption = -1;
  // int totalQuestion = questionList.length;

  void nextQuestion() {
    currentQuestion++;
    update();
  }

  void setSelectedOption(int option) {
    selectedOption = option;
    update();
  }
}
