

import 'package:get/get.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/models/user.dart';
import 'package:quiz/services/database.dart';


class UserController extends GetxController {
  Rx<UserModel> _userModel = UserModel().obs;
  UserModel get user => _userModel.value;
  Rx<List<UserModel>> usersList = Rx<List<UserModel>>();
  List<UserModel> get users => usersList.value;
  set user(UserModel value) => this._userModel.value = value;
  void clear() {
    _userModel.value = UserModel();
  }
  @override
  void onInit() {
    String uid = Get.find<FirebaseController>().auth.currentUser.uid;
    usersList
        .bindStream(DatabaseService().usersStream(uid));

    super.onInit();
  }

}