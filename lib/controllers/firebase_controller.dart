




import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:quiz/constants/strings.dart';
import 'package:quiz/controllers/attempted_quiz_controller.dart';
import 'package:quiz/controllers/quiz_controller.dart';
import 'package:quiz/controllers/user_controller.dart';
import 'package:quiz/services/database.dart';
import 'package:quiz/views/home_page.dart';
import 'package:quiz/views/login_page.dart';
import 'package:quiz/views/splash_screen.dart';
import 'package:quiz/views/welcome_page.dart';



class FirebaseController extends GetxController{

  FirebaseAuth auth = FirebaseAuth.instance;
  // final FacebookLogin fbLogin = new FacebookLogin();
  // GoogleSignIn googleSignIn = GoogleSignIn(scopes: ['email']);
  Rx<User> _firebaseUser = Rx<User>();


  String get user => _firebaseUser.value?.email;
  String get imageurl =>_firebaseUser.value?.photoURL;


  @override
  void onInit() {
    _firebaseUser.bindStream(auth.authStateChanges());
    super.onInit();

  }
  // function to createuser, login and sign out user

  void createUser({String name, String userName,String email,
    String password}) async
  {
    // CollectionReference reference = FirebaseFirestore.instance.collection(USERS_COLLECTION);
    Map<String,dynamic> userdata ={
      NAME:name,
      EMAIL:email,
      USER_NAME:userName,
      LOCATION:" ",
      PROFILE_IMAGE:'https://firebasestorage.googleapis.com/v0/b/quiz-d78e5.appspot.com/o/blank-profile-picture-973460_1280.png?alt=media&token=959e6efd-5a33-4189-bbd2-1f572acbb224'
    };

    await auth.createUserWithEmailAndPassword(email: email, password: password).
    then((value) {
      FirebaseFirestore.instance.collection(USERS_COLLECTION).doc(auth.currentUser.uid)
          .set({
        NAME:name,
        EMAIL:email,
        USER_NAME:userName,
        LOCATION:" ",
        PROFILE_IMAGE:'https://firebasestorage.googleapis.com/v0/b/quiz-d78e5.appspot.com/o/blank-profile-picture-973460_1280.png?alt=media&token=959e6efd-5a33-4189-bbd2-1f572acbb224'
      });
      Get.offAll(HomePage());
      // reference.add(userdata).then((value) =>  Get.offAll(HomeTabScreen()));

    }).catchError((onError)=>
        Get.snackbar("Error while creating account ", onError.message),
    );
  }

   login(String email,String password) async
  {
    // FirebaseAuth auth = FirebaseAuth.instance;

    await auth.signInWithEmailAndPassword(email: email, password: password).then((value)async{
      // Get.put(FirebaseController());
      // print("user logged in");
      // Get.put(UserController()).user   =
      // await DatabaseService().getUser(auth.currentUser.uid);
      // print("user logged in");
      // this.auth=auth;
      // if(Get.find<FirebaseController>().user!=null){
      //  return true;
      // }else{
      //   return false;
      // }
      // Get.offAll(HomePage());

    })

        .then((value) => Get.offAll(HomePage())).
    catchError((onError)=>
        Get.snackbar("Error while sign in ", onError.message));
  }

  signOut() async{

    try {
      auth.signOut().then((value){
        // Get.put(AttemptedQuizController()).clear();
        // Get.find<UserController>().clear();
        // Get.find<FirebaseController>().auth.signOut();
        // Get.put(FirebaseController());
        // auth=FirebaseAuth.instance;
       return Get.offAll(SplashScreen());
      });
    } catch (e) {
      Get.snackbar(
        "Error signing out",
        e.message,
        snackPosition: SnackPosition.BOTTOM,
      );
    }

  }


  void sendpasswordresetemail(String email) async{
    await auth.sendPasswordResetEmail(email: email).then((value) {
      // Get.offAll(SignInScreen());
      Get.snackbar("Password Reset email link is been sent", "Success");
    }).catchError((onError)=> Get.snackbar("Error In Email Reset", onError.message) );
  }

  void deleteuseraccount(String email,String pass) async{
    User user = await auth.currentUser;

    AuthCredential credential = EmailAuthProvider.credential(email: email, password: pass);

    await user.reauthenticateWithCredential(credential).then((value) {
      value.user.delete().then((res) {
        // Get.offAll(SignInScreen());
        Get.snackbar("User Account Deleted ", "Success");
      });
    }

    ).catchError((onError)=> Get.snackbar("Credential Error", "Failed"));
  }

// void google_signIn() async{
//
//   final GoogleSignInAccount googleUser = await googleSignIn.signIn();
//
//   final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
//   final AuthCredential credential = GoogleAuthProvider.getCredential(
//       idToken: googleAuth.idToken,
//       accessToken: googleAuth.accessToken
//   );
//
//   final UserCredential authResult =  await auth.signInWithCredential(credential);
//   Map<String,String> userdata ={
//     // NAME: authResult.user.displayName,
//     // EMAIL:authResult.user.email,
//     // USER_NAME:'',
//     // PHONE_NUMBER:authResult.user.phoneNumber,
//     // GENDER:'',
//     // DATE_OF_BIRTH:null,
//     // COUNTRY:'',
//     // CITY:'',
//     // USER_ID:authResult.user.uid,
//     // DATE_CREATED:DateTime.now().toString(),
//     // POINTS:0.toString(),
//     // JOB_TITLE:'',
//     // COMPANY:'',
//     // WEBSITE_LINK:'',
//     // BIO:'',
//     // COLLEGE:'',
//     // HIGH_SCHOOL:'',
//     // PROFILE_IMAGE:'https://firebasestorage.googleapis.com/v0/b/doer-b61b8.appspot.com/o/default.png?alt=media&token=83d57c95-0f46-4e78-b771-627cd3f57147'
//
//   };
//   // if(authResult.additionalUserInfo.isNewUser){
//   //   FirebaseFirestore.instance.collection(USERS_COLLECTION).
//   //   doc(authResult.user.uid).set(userdata).then((value) async{
//   //     // Get.find<UserController>().user   =
//   //     //     await DatabaseServices().getUser(auth.u.uid);
//   //     Get.offAll(HomeTabScreen());
//   //
//   //   });
//   //
//   // }else{
//   //   Get.offAll(HomeTabScreen());
//   // }
//
//
// }
// void userSignOut() async{
//   await auth.signOut().then((value) => Get.offAll(SignInScreen()));
// }
// void google_signOut() async{
//   await googleSignIn.signOut().then((value) {
//     // Get.offAll(main())
//     main();
//   } );
// }

// facebookLogin() async {
//   User currentUser;
//
//   try {
//     final FacebookLoginResult facebookLoginResult =
//     await fbLogin.logIn(['email', 'public_profile']);
//     if (facebookLoginResult.status == FacebookLoginStatus.loggedIn) {
//       FacebookAccessToken facebookAccessToken =
//           facebookLoginResult.accessToken;
//       final AuthCredential credential = FacebookAuthProvider.credential(facebookAccessToken.token);
//       final UserCredential user = await auth.signInWithCredential(credential);
//
//       assert(user.user.email != null);
//       assert(user.user.displayName != null);
//       assert(!user.user.isAnonymous);
//       assert(await user.user.getIdToken() != null);
//       // currentUser = await auth.currentUser();
//       assert(user.user.uid == currentUser.uid);
//       // return currentUser;
//     }
//   } catch (e) {
//     print(e);
//   }
//
// }
// Future<bool> facebookLogout() async {
//   await auth.signOut();
//   await fbLogin.logOut();
//   return true;
// }
// loginWithTwitter() async {
//   var twitterLogin = new TwitterLogin(
//     apiKey: 'key',
//     apiSecretKey: 'secretkey',
//   );
//
//   final authResult = await twitterLogin.login();
//
//   switch (authResult.status) {
//     case TwitterLoginStatus.loggedIn:
//       final AuthCredential credential = TwitterAuthProvider.getCredential(
//         accessToken: authResult.authToken,
//         secret: authResult.authTokenSecret,
//       );
//       User firebaseUser=(await auth.signInWithCredential(credential)).user;
//       print("twitter sign in"+firebaseUser.toString());
//       break;
//     case TwitterLoginStatus.cancelledByUser:
//       break;
//     case TwitterLoginStatus.error:
//       break;
//   }
//
//   // switch (authResult.status) {
//   //   case TwitterLoginStatus.loggedIn:
//   //   // success
//   //     break;
//   //   case TwitterLoginStatus.cancelledByUser:
//   //   // cancel
//   //     break;
//   //   case TwitterLoginStatus.error:
//   //   // error
//   //     break;
//   // }
//
// }
}