

import 'package:get/get.dart';
import 'package:quiz/controllers/firebase_controller.dart';
import 'package:quiz/models/quiz_model.dart';
import 'package:quiz/services/quiz_services.dart';


class QuizController extends GetxController {
  var isLoading=true.obs;
  Rx<List<QuizModel>> quizList = Rx<List<QuizModel>>();
  List<QuizModel> get quiz => quizList.value;
  var lengthCount=0.obs;
  void clear() {
    quizList.value.clear();
  }
  @override
  void onInit() {
      isLoading(true);
    // String uid = Get.find<FirebaseController>().auth.currentUser.uid;
      quizList
          .bindStream(QuizServices().quizStream());
    isLoading(false);
    super.onInit();

  }

}